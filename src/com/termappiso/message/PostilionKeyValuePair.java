package com.termappiso.message;

import ap.sdk.message.Iso8583JSON;
import ap.sdk.message.KeyValuePair;
import ap.sdk.util.Translate;
import java.util.HashMap;
import java.util.Iterator;

import ap.sdk.util.Utility;

/**
 *
 * @author Abhishek M
 */
public class PostilionKeyValuePair
{

    HashMap<String, String> input;

    /**
     *
     */
    public PostilionKeyValuePair()
    {
        input = new HashMap<String, String>();
    }

    /**
     *
     * @param key
     * @param value
     */
    public void put(String key, String value)
    {
        input.put(key, value);
    }

    /**
     *
     * @return
     */
    public String formMsg()
    {
        Iterator<String> itr = input.keySet().iterator();
        String key;
        String value;
        String len_key;
        String len_value;
        StringBuilder sb = new StringBuilder("");

        while (itr.hasNext())
        {
            key = itr.next();
            len_key = Integer.toString(key.length());
            value = input.get(key);
            len_value = Integer.toString(value.length());

            sb.append(Integer.toString(len_key.length()));
            sb.append(Integer.toString(key.length()));
            sb.append(key);
            sb.append(Integer.toString(len_value.length()));
            sb.append(Integer.toString(value.length()));
            sb.append(value);
        }

        return sb.toString();
    }

    /**
     *
     * @param data
     * @return
     */
    public HashMap<String, String> formHM(String data)
    {
        HashMap<String, String> hm_output = new HashMap<>();

        int key_len;
        int value_len;
        String key;
        String value;

        int startpos = 0;
        int len;

        try
        {
            while (startpos < data.length())
            {
                len = 3;

                key_len = Integer.parseInt(data.substring(startpos, startpos + len));
                startpos = startpos + len;
                len = key_len;
                key = data.substring(startpos, startpos + len);
                startpos = startpos + len;
                len = 5;
                value_len = Integer.parseInt(
                        data.substring(startpos, startpos + len));
                startpos = startpos + len;
                len = value_len;
                value = data.substring(startpos, startpos + len);
                startpos = startpos + len;

                hm_output.put(key, value);
                //input.put(key, value);
            }
        } catch (Exception ex)
        {
            Utility.debugPrint(Utility.getFormattedDateTime(), "Error Parsing Data : "
                    + data + "\n" + ex.getMessage());

            return new HashMap<>();
        }

        return hm_output;
    }

    /**
     *
     * @return
     */
    public HashMap<String, String> getKVPHM()
    {
        return input;
    }

    /**
     *
     * @param args
     */
//    public static void main(String[] args)
//    {
//        String data="007ZIPCODE000040001003SID0001420020212345678009AddlNote100005MN123009AddlNote300003JNB009AddlNote400003DUR009AddlNote500003001003PRD00005C,XXX";
//        Iso8583JSON msg = new Iso8583JSON();
//        AdditionalData ad = new AdditionalData();
//        String posdata;
//        String transactionId;
//        String operatorid = "00000";
//        
//        msg.setField(41, "98920001");
//        msg.setField(62, "880905");
//        msg.setField(120, data);
//
//        transactionId = msg.getField(Iso8583JSON.Bit._062_TRANS_ID);
//
//        if (transactionId != null)
//        {
//            if (transactionId.trim().length() > 6)
//            {
//                transactionId = transactionId.substring(transactionId.trim().length() - 6);
//            }
//        } else
//        {
//            transactionId = "0";
//        }
//
//        transactionId = Utility.resize(transactionId, 6, "0", false);
//        posdata = msg.getField(Iso8583JSON.Bit._041_CARD_ACCEPTOR_TERM_ID) + transactionId + operatorid;
//
//        ad.setField(1, posdata);
//
//        // Structured Data
//        String key;
//        String psd = "";
//        PostilionKeyValuePair pkvp = new PostilionKeyValuePair();
//
//        KeyValuePair kvp = new KeyValuePair();
//        HashMap<String, String> hm_kvp = kvp.formHM(msg.getField(Iso8583JSON.Bit._120_TRAN_DATA_REQ));
//
//        for (int i = 0; i < 10; i++)
//        {
//            key = "AddlNote" + i;
//
//            if (hm_kvp.containsKey(key))
//            {
//                pkvp.put(key, hm_kvp.get(key));
//            }
//        }
//        
//        if(!pkvp.getKVPHM().isEmpty())
//        {
//            psd = pkvp.formMsg();
//                        
//            ad.setField(16, psd);
//        }
//
//        System.out.println(ad.dumpMsg());
//    }
//    }
}
