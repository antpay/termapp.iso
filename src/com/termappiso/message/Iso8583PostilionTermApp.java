package com.termappiso.message;

import ap.sdk.message.Iso8583JSON;
import ap.sdk.message.Iso8583JSON.Bit;
import ap.sdk.message.Iso8583JSON.MsgTypeStr;
import ap.sdk.message.Iso8583JSON.RspCode;
import ap.sdk.message.Iso8583JSON.TranType;
import ap.sdk.message.iso8583.DataFormatter;
import ap.sdk.message.iso8583.Field;
import ap.sdk.message.iso8583.LengthFormatter;
import ap.sdk.message.iso8583.XFieldError;
import ap.sdk.util.FormatData;
import ap.sdk.util.Translate;
import ap.sdk.util.Utility;
import com.termappiso.Config;
import java.util.BitSet;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 * @author abhishek.m
 */
public class Iso8583PostilionTermApp extends Iso8583JSON
{

    // Ascii Length of TPDU
    public static final int _TPDU_LEN = 1;
    String tpdu;
    ConcurrentHashMap<Integer, Field> fieldmap;
    
    public Iso8583PostilionTermApp()
    {
        fieldmap = new ConcurrentHashMap<>(128);
        
        fieldmap.put(0, new Field(new DataFormatter(
                DataFormatter.DataType._HEX, "", false, true, 4),
                new LengthFormatter(LengthFormatter.LengthType._FIXED_LEN,
                        LengthFormatter.LengthFormat._NONE, false, 4)));
        fieldmap.put(1, new Field(new DataFormatter(
                DataFormatter.DataType._BINARY, "", false, true, 32),
                new LengthFormatter(LengthFormatter.LengthType._FIXED_LEN,
                        LengthFormatter.LengthFormat._NONE, false, 32)));
        fieldmap.put(Iso8583JSON.Bit._002_PAN, new Field(new DataFormatter(
                DataFormatter.DataType._HEX, "F", true, false, 50),
                new LengthFormatter(LengthFormatter.LengthType._LLVAR,
                        LengthFormatter.LengthFormat._HEX, false, 50)));
        fieldmap.put(Iso8583JSON.Bit._003_PROCESSING_CODE, new Field(
                new DataFormatter(DataFormatter.DataType._HEX, "0", true,
                        true, 6), new LengthFormatter(
                        LengthFormatter.LengthType._FIXED_LEN,
                        LengthFormatter.LengthFormat._HEX, false, 6)));
        fieldmap.put(Iso8583JSON.Bit._004_AMOUNT_TRANSACTION, new Field(
                new DataFormatter(DataFormatter.DataType._HEX, "0", true,
                        true, 12), new LengthFormatter(
                        LengthFormatter.LengthType._FIXED_LEN,
                        LengthFormatter.LengthFormat._HEX, false, 12)));
        fieldmap.put(Iso8583JSON.Bit._007_TRANSMISSION_DATE_TIME, new Field(
                new DataFormatter(DataFormatter.DataType._HEX, "0", true,
                        true, 10), new LengthFormatter(
                        LengthFormatter.LengthType._FIXED_LEN,
                        LengthFormatter.LengthFormat._HEX, false, 10)));
        fieldmap.put(Iso8583JSON.Bit._011_SYSTEMS_TRACE_AUDIT_NR, new Field(
                new DataFormatter(DataFormatter.DataType._HEX, "0", true,
                        true, 06), new LengthFormatter(
                        LengthFormatter.LengthType._FIXED_LEN,
                        LengthFormatter.LengthFormat._HEX, false, 06)));
        fieldmap.put(RemoteBit._012_DATE_TIME_LOCAL, new Field(
                new DataFormatter(DataFormatter.DataType._HEX, "0", true,
                        true, 12), new LengthFormatter(
                        LengthFormatter.LengthType._FIXED_LEN,
                        LengthFormatter.LengthFormat._HEX, false, 12)));
        fieldmap.put(Iso8583JSON.Bit._014_DATE_EXPIRATION, new Field(
                new DataFormatter(DataFormatter.DataType._HEX, "0", true,
                        true, 4), new LengthFormatter(
                        LengthFormatter.LengthType._FIXED_LEN,
                        LengthFormatter.LengthFormat._HEX, false, 4)));
        fieldmap.put(Iso8583JSON.Bit._016_DATE_CONV, new Field(
                new DataFormatter(DataFormatter.DataType._HEX, "0", true,
                        true, 4), new LengthFormatter(
                        LengthFormatter.LengthType._FIXED_LEN,
                        LengthFormatter.LengthFormat._HEX, false, 4)));
        fieldmap.put(Iso8583JSON.Bit._018_MERCHANT_TYPE, new Field(
                new DataFormatter(DataFormatter.DataType._HEX, "0", true, true,
                        4), new LengthFormatter(
                        LengthFormatter.LengthType._FIXED_LEN,
                        LengthFormatter.LengthFormat._HEX, false, 4)));
        fieldmap.put(RemoteBit._022_POS_DATA_CODE, new Field(
                new DataFormatter(DataFormatter.DataType._HEX, "0", false,
                        true, 15), new LengthFormatter(
                        LengthFormatter.LengthType._FIXED_LEN,
                        LengthFormatter.LengthFormat._HEX, false, 15)));
        fieldmap.put(Iso8583JSON.Bit._023_CARD_SEQ_NR, new Field(
                new DataFormatter(DataFormatter.DataType._HEX, "0", false,
                        true, 3), new LengthFormatter(
                        LengthFormatter.LengthType._FIXED_LEN,
                        LengthFormatter.LengthFormat._HEX, false, 3)));
        fieldmap.put(Iso8583JSON.Bit._024_NETWORK_INTL_ID, new Field(
                new DataFormatter(DataFormatter.DataType._HEX, "0", false,
                        true, 3), new LengthFormatter(
                        LengthFormatter.LengthType._FIXED_LEN,
                        LengthFormatter.LengthFormat._HEX, false, 3)));
        fieldmap.put(RemoteBit._025_MSG_REASON_CODE, new Field(
                new DataFormatter(DataFormatter.DataType._HEX, "0", true,
                        true, 2), new LengthFormatter(
                        LengthFormatter.LengthType._FIXED_LEN,
                        LengthFormatter.LengthFormat._HEX, false, 2)));
        fieldmap.put(RemoteBit._028_AMOUNT_TRAN_FEE, new Field(
                new DataFormatter(DataFormatter.DataType._HEX, "0", false,
                        true, 6), new LengthFormatter(
                        LengthFormatter.LengthType._FIXED_LEN,
                        LengthFormatter.LengthFormat._HEX, false, 6)));
        fieldmap.put(RemoteBit._029_RECON_INDICATOR, new Field(
                new DataFormatter(DataFormatter.DataType._HEX, "0", true,
                        true, 3), new LengthFormatter(
                        LengthFormatter.LengthType._FIXED_LEN,
                        LengthFormatter.LengthFormat._HEX, false, 3)));
        fieldmap.put(RemoteBit._030_AMOUNTS_ORIGINAL, new Field(
                new DataFormatter(DataFormatter.DataType._HEX, "0", true,
                        true, 2), new LengthFormatter(
                        LengthFormatter.LengthType._FIXED_LEN,
                        LengthFormatter.LengthFormat._HEX, false, 2)));
        fieldmap.put(Iso8583JSON.Bit._032_ACQUIRING_INST_ID_CODE, new Field(
                new DataFormatter(DataFormatter.DataType._HEX, "0", true,
                        false, 11), new LengthFormatter(
                        LengthFormatter.LengthType._LLVAR,
                        LengthFormatter.LengthFormat._HEX, false, 11)));
        fieldmap.put(Iso8583JSON.Bit._033_FORWARDING_INST_ID_CODE, new Field(
                new DataFormatter(DataFormatter.DataType._HEX, "0", true,
                        false, 11), new LengthFormatter(
                        LengthFormatter.LengthType._LLVAR,
                        LengthFormatter.LengthFormat._HEX, false, 11)));
        fieldmap.put(Iso8583JSON.Bit._035_TRACK_2_DATA, new Field(
                new DataFormatter(DataFormatter.DataType._HEX, "0", false,
                        false, 80), new LengthFormatter(
                        LengthFormatter.LengthType._LLVAR,
                        LengthFormatter.LengthFormat._HEX, false, 80)));
        fieldmap.put(Iso8583JSON.Bit._037_RETRIEVAL_REF_NR, new Field(
                new DataFormatter(DataFormatter.DataType._HEX, "0", true, true,
                        12), new LengthFormatter(
                        LengthFormatter.LengthType._FIXED_LEN,
                        LengthFormatter.LengthFormat._HEX, false, 12)));
        fieldmap.put(Iso8583JSON.Bit._038_AUTH_ID_RSP, new Field(
                new DataFormatter(DataFormatter.DataType._HEX, " ", false,
                        true, 6), new LengthFormatter(
                        LengthFormatter.LengthType._FIXED_LEN,
                        LengthFormatter.LengthFormat._HEX, false, 6)));
        fieldmap.put(Iso8583JSON.Bit._039_RSP_CODE, new Field(
                new DataFormatter(DataFormatter.DataType._HEX, "", false, true,
                        3), new LengthFormatter(
                        LengthFormatter.LengthType._FIXED_LEN,
                        LengthFormatter.LengthFormat._HEX, false, 3)));
        fieldmap.put(Iso8583JSON.Bit._040_SERVICE_RESTRICTION_CODE, new Field(
                new DataFormatter(DataFormatter.DataType._HEX, "", false, true,
                        3), new LengthFormatter(
                        LengthFormatter.LengthType._FIXED_LEN,
                        LengthFormatter.LengthFormat._HEX, false, 3)));
        fieldmap.put(Iso8583JSON.Bit._041_CARD_ACCEPTOR_TERM_ID, new Field(
                new DataFormatter(DataFormatter.DataType._HEX, " ", true, true,
                        8), new LengthFormatter(
                        LengthFormatter.LengthType._FIXED_LEN,
                        LengthFormatter.LengthFormat._HEX, false, 8)));
        fieldmap.put(Iso8583JSON.Bit._042_CARD_ACCEPTOR_ID_CODE, new Field(
                new DataFormatter(DataFormatter.DataType._HEX, " ", true, true,
                        15), new LengthFormatter(
                        LengthFormatter.LengthType._FIXED_LEN,
                        LengthFormatter.LengthFormat._HEX, false, 15)));
        fieldmap.put(Iso8583JSON.Bit._043_CARD_ACCEPTOR_NAME_LOC, new Field(
                new DataFormatter(DataFormatter.DataType._HEX, " ", false,
                        true, 40), new LengthFormatter(
                        LengthFormatter.LengthType._FIXED_LEN,
                        LengthFormatter.LengthFormat._HEX, false, 40)));
        fieldmap.put(Iso8583JSON.Bit._044_ADDITIONAL_RSP_DATA, new Field(
                new DataFormatter(DataFormatter.DataType._HEX, "0", true,
                        false, 99), new LengthFormatter(
                        LengthFormatter.LengthType._LLVAR,
                        LengthFormatter.LengthFormat._HEX, false, 99)));
        fieldmap.put(Iso8583JSON.Bit._045_TRACK_1_DATA, new Field(
                new DataFormatter(DataFormatter.DataType._HEX, " ", false,
                        false, 99), new LengthFormatter(
                        LengthFormatter.LengthType._LLVAR,
                        LengthFormatter.LengthFormat._HEX, false, 99)));
        fieldmap.put(Iso8583JSON.Bit._046_ADDITIONAL_DATA_ISO, new Field(
                new DataFormatter(DataFormatter.DataType._HEX, " ", false,
                        false, 204), new LengthFormatter(
                        LengthFormatter.LengthType._LLLVAR,
                        LengthFormatter.LengthFormat._HEX, false, 204)));
        fieldmap.put(Iso8583JSON.Bit._048_ADDITIONAL_DATA, new Field(
                new DataFormatter(DataFormatter.DataType._HEX, " ", false,
                        false, 9999), new LengthFormatter(
                        LengthFormatter.LengthType._LLLLVAR,
                        LengthFormatter.LengthFormat._HEX, false, 9999)));
        fieldmap.put(Iso8583JSON.Bit._049_CURRENCY_CODE_TRAN, new Field(
                new DataFormatter(DataFormatter.DataType._HEX, "0", true, true,
                        3), new LengthFormatter(
                        LengthFormatter.LengthType._FIXED_LEN,
                        LengthFormatter.LengthFormat._HEX, false, 3)));
        fieldmap.put(Iso8583JSON.Bit._052_PIN_DATA, new Field(
                new DataFormatter(DataFormatter.DataType._HEX, "0", false,
                        true, 8), new LengthFormatter(
                        LengthFormatter.LengthType._FIXED_LEN,
                        LengthFormatter.LengthFormat._HEX, false, 8)));
        fieldmap.put(Iso8583JSON.Bit._053_SECURITY_INFO, new Field(
                new DataFormatter(DataFormatter.DataType._HEX, " ", false,
                        false, 96), new LengthFormatter(
                        LengthFormatter.LengthType._LLVAR,
                        LengthFormatter.LengthFormat._HEX, false, 96)));
        fieldmap.put(RemoteBit._054_ADDITIONAL_AMOUNTS, new Field(
                new DataFormatter(DataFormatter.DataType._HEX, "0", false,
                        true, 120), new LengthFormatter(
                        LengthFormatter.LengthType._LLLVAR,
                        LengthFormatter.LengthFormat._HEX, false, 120)));
        fieldmap.put(Iso8583JSON.Bit._055_EMV_DATA, new Field(
                new DataFormatter(DataFormatter.DataType._HEX, " ", false,
                        false, 255), new LengthFormatter(
                        LengthFormatter.LengthType._LLLVAR,
                        LengthFormatter.LengthFormat._HEX, false, 999)));
        fieldmap.put(56, new Field(
                new DataFormatter(DataFormatter.DataType._HEX, " ", false,
                        false, 31), new LengthFormatter(
                        LengthFormatter.LengthType._LLVAR,
                        LengthFormatter.LengthFormat._HEX, false, 31)));
        fieldmap.put(Iso8583JSON.Bit._060_ADVICE_REASON_CODE, new Field(
                new DataFormatter(DataFormatter.DataType._HEX, " ", false,
                        false, 999), new LengthFormatter(
                        LengthFormatter.LengthType._LLLVAR,
                        LengthFormatter.LengthFormat._HEX, false, 999)));
        fieldmap.put(Iso8583JSON.Bit._061_POS_DATA, new Field(
                new DataFormatter(DataFormatter.DataType._HEX, " ", false,
                        false, 999), new LengthFormatter(
                        LengthFormatter.LengthType._LLLVAR,
                        LengthFormatter.LengthFormat._HEX, false, 999)));
        fieldmap.put(Iso8583JSON.Bit._062_TRANS_ID, new Field(
                new DataFormatter(DataFormatter.DataType._HEX, " ", false,
                        false, 999), new LengthFormatter(
                        LengthFormatter.LengthType._LLLVAR,
                        LengthFormatter.LengthFormat._HEX, false, 999)));
        fieldmap.put(Iso8583JSON.Bit._063_PRIVATE_USE, new Field(
                new DataFormatter(DataFormatter.DataType._HEX, " ", false,
                        false, 999), new LengthFormatter(
                        LengthFormatter.LengthType._LLLVAR,
                        LengthFormatter.LengthFormat._HEX, false, 999)));
        fieldmap.put(Iso8583JSON.Bit._070_NETWORK_MNG_INFO_CODE, new Field(
                new DataFormatter(DataFormatter.DataType._HEX, "0", true, true,
                        3), new LengthFormatter(
                        LengthFormatter.LengthType._FIXED_LEN,
                        LengthFormatter.LengthFormat._HEX, false, 3)));
        fieldmap.put(Iso8583JSON.Bit._090_ORIGINAL_DATA_ELEMENTS, new Field(
                new DataFormatter(DataFormatter.DataType._HEX, " ", true, true,
                        42), new LengthFormatter(
                        LengthFormatter.LengthType._FIXED_LEN,
                        LengthFormatter.LengthFormat._HEX, false, 42)));
        fieldmap.put(Iso8583JSON.Bit._095_REPLACEMENT_AMOUNTS, new Field(
                new DataFormatter(DataFormatter.DataType._HEX, " ", false,
                        true, 42), new LengthFormatter(
                        LengthFormatter.LengthType._FIXED_LEN,
                        LengthFormatter.LengthFormat._HEX, false, 42)));
        fieldmap.put(Iso8583JSON.Bit._123_EXTENDED_FIELD, new Field(
                new DataFormatter(DataFormatter.DataType._HEX, " ", false,
                        false, 999), new LengthFormatter(
                        LengthFormatter.LengthType._LLLVAR,
                        LengthFormatter.LengthFormat._HEX, false, 999)));
    }

    /**
     *
     * @return The Hashmap containing the ISO fields
     */
    public ConcurrentHashMap<Integer, Field> getIsoFields()
    {
        return fieldmap;
    }

    /**
     *
     * @param fieldmap
     */
    public void setIsoFields(ConcurrentHashMap<Integer, Field> fieldmap)
    {
        this.fieldmap = fieldmap;
    }

    /**
     *
     * @return
     */
    public byte[] getTPDU()
    {
        return Translate.getData(tpdu);
    }
    
    public String getFormattedTPDU()
    {
        return tpdu;
    }

    /**
     *
     * @param htpdu
     */
    public void setTPDU(String htpdu)
    {
        tpdu = htpdu;
    }

    /**
     *
     */
    /**
     * @param swaptpdu
     * @return
     * @throws XFieldError
     */
    public byte[] getMessage() throws XFieldError
    {
        
        Field f;
        String fieldvalue;
        byte[] msg;
        byte[] temp;
        int totallen = 0;
        int pos = 0;
        BitSet bitmap = new BitSet();
        
        for (int i = 0; i <= 128; i++)
        {
            if (i != 1)
            {
                if (i == 0)
                {
                    fieldvalue = getMsgType();
                } else
                {
                    fieldvalue = getField(i);
                }
                
                if (fieldvalue != null && fieldvalue.length() > 0)
                {
                    f = fieldmap.get(i);
                    
                    f.setContent(fieldvalue);
                    temp = f.getFormattedContent();
                    
                    totallen = totallen + temp.length;
                    
                    bitmap.set(i);
                    
                    if (i > 64)
                    {
                        bitmap.set(1);
                    }
                }
            }
        }
        
        byte[] byte_bitmap = FormatData.bitSet2byte(bitmap);
        String a;
        
        if (tpdu.equals("A"))
        {
            a = Translate.fromBinToHex(Translate.getString(byte_bitmap));
        } else
        {
            a = Translate.getString(byte_bitmap);
        }
        byte[] n_byte_bitmap = Translate.getData(a);
        totallen = totallen + n_byte_bitmap.length + _TPDU_LEN;
        
        msg = new byte[totallen];
        
        System.arraycopy(getTPDU(), 0, msg, 0, _TPDU_LEN);
        pos = pos + _TPDU_LEN;
        
        for (int i = 0; i <= 128; i++)
        {
            if (i != 1)
            {
                if (i == 0)
                {
                    fieldvalue = getMsgType();
                } else
                {
                    fieldvalue = getField(i);
                }
                
                if (fieldvalue != null && fieldvalue.length() > 0)
                {
                    f = fieldmap.get(i);
                    
                    f.setContent(fieldvalue);
                    temp = f.getFormattedContent();
                    System.arraycopy(temp, 0, msg, pos, temp.length);
                    pos = pos + temp.length;
                }
            } else
            {
                System.arraycopy(n_byte_bitmap, 0, msg, pos, n_byte_bitmap.length);
                pos = pos + n_byte_bitmap.length;
            }
        }
        
        return msg;
    }

    /**
     * Parses The Message From Binary To Json String As Per ISO8583 Standards
     *
     * @param data
     * @throws Exception
     */
    public void parseIso8583Msg(byte[] data) throws Exception
    {
        Field f;
        int flen;
        byte[] temp;
        byte[] temp_len;
        int pos = 0;
        int len_of_len;
        byte[] b_tpdu = new byte[1];

        // first we read the TPDU
        System.arraycopy(data, 0, b_tpdu, 0, _TPDU_LEN);
        
        tpdu = Translate.getString(b_tpdu);

        // Calculate the Message Offset here
        pos = pos + _TPDU_LEN;

        // first we read the MTI
        f = fieldmap.get(0);
        if (f.getFieldLengthType() == LengthFormatter.LengthType._FIXED_LEN)
        {
            flen = f.getFieldDataParseMsgLen();
            
            if (f.getFieldDataType() == DataFormatter.DataType._BINARY)
            {
                temp = new byte[flen];
                System.arraycopy(data, pos, temp, 0, flen);
                setMsgType(Translate.fromBinToHex(Translate.getString(temp)));
            } else if (f.getFieldDataType() == DataFormatter.DataType._EBCDIC)
            {
                temp = new byte[flen];
                System.arraycopy(data, pos, temp, 0, flen);
                setMsgType(
                        Translate.fromEbcdicToAscii(Translate.getString(temp)));
            } else if (f.getFieldDataType() == DataFormatter.DataType._HEX)
            {
                temp = new byte[flen];
                System.arraycopy(data, pos, temp, 0, flen);
                setMsgType(Translate.getString(temp));
            }
            
            pos = pos + flen;
        }

        // read the Bitmap
        f = fieldmap.get(1);
        byte[] bmap_data = new byte[32];
        
        System.arraycopy(data, pos, bmap_data, 0, 32);
        
        BitSet bitmap;
        if (tpdu.equals("A"))
        {
            bitmap = FormatData.byte2BitSet(
                    Translate.getData(Translate.fromHexToBin(Translate.getString(bmap_data))),
                    0, 128);
        } else
        {
            bitmap = FormatData.byte2BitSet(
                    Translate.getData(Translate.getString(bmap_data)),
                    0, 128);
        }
        if (bitmap.get(1))
        {
            flen = 32;
        } else
        {
            flen = 16;
        }
        
        if (f.getFieldDataType() == DataFormatter.DataType._BINARY)
        {
            flen = flen / 2;
        }
        
        pos = pos + flen;

        // process for the remaining fields
        for (int i = 2; i <= 128; i++)
        {
            if (bitmap.get(i))
            {
                f = fieldmap.get(i);
                
                if (f == null)
                {
                    System.out.println("Error Field Parsing - Field not found:" + i);
                    throw new XFieldError("parseIso8583Msg", "Field",
                            "Value : " + i, "Field not defined");
                }
                
                if (f.getFieldLengthType()
                        == LengthFormatter.LengthType._FIXED_LEN)
                {
                    flen = f.getFieldDataParseMsgLen();
                    
                    if (f.getFieldDataType() == DataFormatter.DataType._BINARY)
                    {
                        temp = new byte[flen];
                        System.arraycopy(data, pos, temp, 0, flen);
                        String temp_str = Translate.fromBinToHex(Translate.
                                getString(temp));
                        int act_len = f.getFieldDataLength();
                        temp_str = Utility.removePadChars(temp_str, f.getDf().
                                isPadRight(), act_len);
                        setField(i, temp_str);
                    } else if (f.getFieldDataType()
                            == DataFormatter.DataType._EBCDIC)
                    {
                        temp = new byte[flen];
                        System.arraycopy(data, pos, temp, 0, flen);
                        setField(i, Translate.fromEbcdicToAscii(Translate.
                                getString(temp)));
                    } else if (f.getFieldDataType()
                            == DataFormatter.DataType._HEX)
                    {
                        temp = new byte[flen];
                        System.arraycopy(data, pos, temp, 0, flen);
                        setField(i, Translate.getString(temp));
                    }
                    pos = pos + flen;
                } else
                {
                    flen = f.getFieldParseLength();
                    temp_len = new byte[flen];
                    System.arraycopy(data, pos, temp_len, 0, flen);
                    
                    if (f.getFieldLengthFormat()
                            == LengthFormatter.LengthFormat._BINARY)
                    {
                        len_of_len = Integer.parseInt(Translate.fromBinToHex(
                                Translate.
                                        getString(temp_len)));
                    } else if (f.getFieldLengthFormat()
                            == LengthFormatter.LengthFormat._EBCDIC)
                    {
                        len_of_len = Integer.parseInt(Translate.
                                fromEbcdicToAscii(
                                        Translate.
                                                getString(temp_len)));
                    } else if (f.getFieldLengthFormat()
                            == LengthFormatter.LengthFormat._HEX)
                    {
                        len_of_len = Integer.parseInt(Translate.getString(
                                temp_len));
                    } else
                    {
                        len_of_len = Integer.parseInt(Translate.getString(
                                temp_len));
                    }
                    
                    pos = pos + flen;
                    
                    if (f.getFieldDataType() == DataFormatter.DataType._BINARY)
                    {
                        int act_len = len_of_len;
                        if (Utility.isOdd(len_of_len))
                        {
                            len_of_len = (len_of_len + 1) / 2;
                        } else
                        {
                            len_of_len = len_of_len / 2;
                        }
                        temp = new byte[len_of_len];
                        System.arraycopy(data, pos, temp, 0, len_of_len);
                        String temp_str = Translate.fromBinToHex(Translate.
                                getString(temp));
                        temp_str = Utility.removePadChars(temp_str, f.getDf().
                                isPadRight(), act_len);
                        setField(i, temp_str);
                    } else if (f.getFieldDataType()
                            == DataFormatter.DataType._EBCDIC)
                    {
                        temp = new byte[len_of_len];
                        System.arraycopy(data, pos, temp, 0, len_of_len);
                        setField(i, Translate.fromEbcdicToAscii(Translate.
                                getString(temp)));
                    } else if (f.getFieldDataType()
                            == DataFormatter.DataType._HEX)
                    {
                        temp = new byte[len_of_len];
                        System.arraycopy(data, pos, temp, 0, len_of_len);
                        setField(i, Translate.getString(temp));
                    }
                    pos = pos + len_of_len;
                }
            }
        }
    }

    /**
     *
     * @param data
     * @throws Exception
     */
    public String parseIso8583Msg2(byte[] data) throws Exception
    {
        //Field f;
        byte[] temp = new byte[data.length - _TPDU_LEN];
        int pos = 0;

        // first we read the TPDU
        System.arraycopy(data, 0, tpdu, 0, _TPDU_LEN);

        // Calculate the Message Offset here
        pos = pos + _TPDU_LEN;
        
        System.arraycopy(data, _TPDU_LEN, temp, 0, data.length - _TPDU_LEN);
        
        String data_str = Translate.getString(temp);
        
        return data_str;
    }

    /**
     *
     * @author Abhishek M
     *
     */
    public class RemoteBit extends Bit
    {
        
        public static final int _012_DATE_TIME_LOCAL = 12;
        public static final int _022_POS_DATA_CODE = 22;
        public static final int _025_MSG_REASON_CODE = 25;
        public static final int _029_RECON_INDICATOR = 29;
        public static final int _030_AMOUNTS_ORIGINAL = 30;
        public static final int _048_ADDITIONAL_DATA = 48;
        public static final int _055_EMV_TAGS = 55;
        public static final int _056_BATCH_NR = 56;
        public static final int _060_PRIVATE_USE = 60;
        public static final int _061_PRIVATE_USE = 61;
        public static final int _062_PRIVATE_USE = 62;
        public static final int _063_PRIVATE_USE = 63;
    }

    /**
     *
     * @author Abhishek M
     *
     */
    public class RemoteRspCode extends RspCode
    {
        
        public static final String _89_BAD_TERMINAL_ID = "89";
    }

    /**
     *
     * @author Abhishek M
     *
     */
    public static class RemoteTranType extends TranType
    {

        // Gift Card Transaction Types
        public static final String _0200_GC_ACTIVATE = "61";
        public static final String _0200_00_SALE = "00";
        public static final String _0200_02_VOID_SALE = "02";
        public static final String _0200_09_SALE_AND_CASH = "09";
        public static final String _0200_20_REFUND = "20";
        public static final String _0200_GC_LOAD = "62";
        public static final String _0200_GC_DEACTIVATE = "63";
        public static final String _0200_31_BALANCE_INQUIRY = "31";
        //Reversal Request Transaction Types
        public static final String _0400_20_REVERSAL_REQUEST = "00)";
        // Settlement Transaction Types
        public static final String _0500_92_SETTLEMENT_REQUEST = "92";
        public static final String _0500_95_BATCH_DOWN_LINE_LOAD = "95";
        public static final String _0500_96_SETTLEMENT_AFTER_UPLOAD = "96";
        // Network Transaction Types
        public static final String _0800_91_STATISTICS = "91";
        public static final String _0800_92_LOGON = "92";
        public static final String _0800_93_INITIALIZATION = "93";
        public static final String _0800_95_HELP_NEEDED = "95";
        public static final String _0800_96_RKI = "96";
        public static final String _0800_97_KEY_MAPPING = "97";
        public static final String _0800_99_TEST_TRANSACTION = "99";
    }

    /**
     *
     * @author Abhishek M
     *
     */
    public static class RemotePOSEntryMode
    {
        // PAN Entry Mode

        public static class RemotePANEntryMode
        {
            
            public static final String _00_UNSPECIFIED = "00";
            public static final String _01_MANUAL = "01";
            public static final String _02_MAGNETIC_STRIPE = "02";
            public static final String _05_ICC_READ = "05";
            public static final String _80_FALLBACK = "80";
            public static final String _90_MAGNETIC_STRIPE = "90";
        }

        // PIN Entry Capability
        public static class RemotePINEntryCapability
        {
            
            public static final String _0_UNSPECIFIED = "0";
            public static final String _1_PIN_ENTRY_CAPABILITY = "1";
            public static final String _2_NO_PIN_ENTRY_CAPABILITY = "2";
        }
    }
    
    public static class PUNGRAIN
    {
        
        public static class TranTypes
        {
            
            public static final String _6801_LABOUR = "6801";
            public static final String _6802_TRANSPORTATION = "6802";
            public static final String _6803_BARDANA_ISSUANCE = "6803";
            public static final String _6804_PVT_PURCHASE = "6804";
            public static final String _6806_BARDANA_RETURN = "6806";
            public static final String _6807_PADDY_VERIFICATION = "6807";
            public static final String _6808_BASMATI_PURCHASE = "6808";
        }
    }

    /**
     *
     * @author Abhishek M
     *
     */
    public static class RemotePosCondCode
    {
        
        public static final String _00_NORMAL_PRESENTMENT = "00";
        public static final String _01_CUSTOMER_NOT_PRESENT = "01";
        public static final String _03_MERCHANT_SUSPICIOUS = "03";
        public static final String _04_ECR_INTERFACE = "04";
        public static final String _05_CARD_NOT_PRESENT = "05";
        public static final String _06_PREAUTHORIZED_REQUEST = "06";
        public static final String _08_MAILORDER_TELEPHONEORDER = "08";
        public static final String _51_OPEN_TAB = "51";
        public static final String _71_CARD_PRESENT_MAGSTRIPE_BAD = "71";
        public static final String _87_STORE_AND_FORWARD = "87";
    }

    /**
     *
     * @author Abhishek M
     *
     */
    public class RemoteMsgType extends MsgTypeStr
    {
        
        public static final String _0304_MERCHANT_MSG_DOWNLOAD = "0304";
    }

    /**
     *
     * @return
     */
    @Override
    public String dumpMsg()
    {
        StringBuilder str = new StringBuilder("");
        String value;
        
        str.append("\nEncoding \t[").append(getFormattedTPDU()).append("]\n");
        str.append(getMsgType()).append(":\n");

        // print all the fields except Extended Field 123
        for (int i = 2; i < 129; i++)
        {
            if (i != 123)
            {
                value = getField(i);
                
                if (i == Iso8583JSON.Bit._002_PAN || i
                        == Iso8583JSON.Bit._035_TRACK_2_DATA)
                {
                    if (value != null && value.trim().length() > 0)
                    {
                        str.append("\t[F").append(Utility.resize(Integer.
                                toString(i), 3, "0",
                                false)).append("] = [").append(FormatData.
                                protect(getField(i))).append("]\n");
                    }
                } else if (i == Iso8583JSON.Bit._045_TRACK_1_DATA)
                {
                    if (value != null && value.trim().length() > 0)
                    {
                        String temp = getField(i);
                        temp = Utility.resize("*", temp.length(), "*", true);
                        str.append("\t[F").append(Utility.resize(Integer.
                                toString(i), 3, "0",
                                false)).append("] = [").append(temp).append(
                                "]\n");
                    }
                } else if (i == Iso8583JSON.Bit._048_ADDITIONAL_DATA)
                {
                    if (value != null && value.trim().length() > 0)
                    {
                        try
                        {
                            String f048_data = getField(i);
                            AdditionalData f048_addl = new AdditionalData();
                            f048_addl.parseMsg(Translate.getData(f048_data));
                            str.append("\t[F").append(Utility.resize(Integer.
                                    toString(i), 3, "0",
                                    false)).append("] =*[\n").append(f048_addl.dumpMsg()).append("\t\t]\n");
                        } catch (Exception ex)
                        {
                            Config.logger.error(ex);
                        }
                    }
                } else if (i == Iso8583JSON.Bit._052_PIN_DATA)
                {
                    if (value != null && value.trim().length() > 0)
                    {
                        str.append("\t[F").append(Utility.resize(Integer.
                                toString(i), 3, "0",
                                false)).append("] = [****************]\n");
                    }
                } else if (i == Iso8583JSON.Bit._053_SECURITY_INFO)
                {
                    if (value != null && value.trim().length() > 0)
                    {
                        str.append("\t[F").append(Utility.resize(Integer.
                                toString(i), 3, "0",
                                false)).append("] =*[").append(Translate.
                                fromBinToHex(getField(i))).append("]\n");
                    }
                } else if (i == Iso8583JSON.Bit._055_EMV_DATA)
                {
                    if (value != null && value.trim().length() > 0)
                    {
                        str.append("\t[F").append(Utility.resize(Integer.
                                toString(i), 3, "0",
                                false)).append("] = [").append(Translate.
                                fromBinToHex(getField(i))).append("]\n");
                    }
                } else if (value != null && value.trim().length() > 0)
                {
                    str.append("\t[F").append(Utility.resize(Integer.
                            toString(i), 3, "0",
                            false)).append("] = [").append(getField(i)).
                            append("]\n");
                }
            }
        }
        
        return str.toString();
    }

    /**
     *
     */
    protected void clearRespFields()
    {
        removeField(Bit._002_PAN);
        removeField(Bit._014_DATE_EXPIRATION);
        removeField(Bit._022_POS_ENTRY_MODE);
        removeField(Bit._024_NETWORK_INTL_ID);
        removeField(Bit._025_POS_CONDITION_CODE);
        removeField(Bit._035_TRACK_2_DATA);
        removeField(Bit._042_CARD_ACCEPTOR_ID_CODE);
        removeField(RemoteBit._044_ADDITIONAL_RSP_DATA);
        removeField(Bit._048_ADDITIONAL_DATA);
        removeField(Bit._052_PIN_DATA);
        removeField(Bit._055_EMV_DATA);
        removeField(RemoteBit._056_BATCH_NR);
        removeField(Bit._060_ADVICE_REASON_CODE);
        removeField(Bit._062_TRANS_ID);
    }

    /**
     *
     * @param args
     */
//    public static void main(String[] args) throws XFieldError, Exception
//    {
//        Iso8583PostilionTermApp iso = new Iso8583PostilionTermApp();
//
//        iso.setTPDU("B");
//        iso.setMsgType("1200");
//        iso.setField(Iso8583PostilionTermApp.Bit._003_PROCESSING_CODE, "000000");
//        iso.setField(Iso8583PostilionTermApp.Bit._004_AMOUNT_TRANSACTION, "000000123456");
//        iso.setField(Iso8583PostilionTermApp.Bit._007_TRANSMISSION_DATE_TIME, Utility.getDateTime());
//        iso.setField(Iso8583PostilionTermApp.Bit._011_SYSTEMS_TRACE_AUDIT_NR, "000001");
//        iso.setField(Iso8583PostilionTermApp.Bit._012_TIME_LOCAL, Utility.getDate().substring(2) + Utility.getTime());
//        iso.setField(Iso8583PostilionTermApp.RemoteBit._022_POS_DATA_CODE, "51010151414C127");
//        iso.setField(Iso8583PostilionTermApp.Bit._024_NETWORK_INTL_ID, "200");
//        iso.setField(Iso8583PostilionTermApp.Bit._029_AMOUNT_SETTLE_FEE, "123");
//        iso.setField(Iso8583PostilionTermApp.Bit._035_TRACK_2_DATA, "7777750000000001D2012126111");
//        iso.setField(Iso8583PostilionTermApp.Bit._041_CARD_ACCEPTOR_TERM_ID, "TID00001");
//        iso.setField(Iso8583PostilionTermApp.Bit._042_CARD_ACCEPTOR_ID_CODE, "MID000000000001");
//
//        AdditionalData ad = new AdditionalData();
//        ad.setField(1, "1234567800000100000");
//
//        iso.setField(48, Translate.getString(ad.getMessage()));
//
//        iso.setField(Iso8583PostilionTermApp.Bit._049_CURRENCY_CODE_TRAN, "710");
//        iso.setField(Iso8583PostilionTermApp.Bit._052_PIN_DATA,
//                Translate.fromHexToBin("0123456789ABCDEF"));
//        iso.setField(Iso8583PostilionTermApp.Bit._053_SECURITY_INFO,
//                Translate.fromHexToBin("20040210820617E0000B000000000000000000000000000020040210820617E0000B0000000000000000000000000000"));
//
//        System.out.println(iso.dumpMsg());
//        System.out.println(FormatData.hexdump(iso.getMessage()));
//
//        byte[] ax = iso.getMessage();
//
//        Iso8583PostilionTermApp rsp = new Iso8583PostilionTermApp();
//
//        try
//        {
//            rsp.parseIso8583Msg(ax);
//        } catch (Exception ex)
//        {
//            ex.printStackTrace();
//        }
//
//        System.out.println(rsp.dumpMsg());
//        
//        System.out.println(Translate.fromHexToBin("4D617374657243617264"));
//    }
}
