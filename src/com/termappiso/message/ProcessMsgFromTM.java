package com.termappiso.message;

import ap.sdk.comms.XHttpServer;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
import ap.sdk.emv.Tag;
import ap.sdk.emv.Tlv;
import ap.sdk.message.Iso8583JSON;
import ap.sdk.message.KeyValuePair;
import ap.sdk.message.OriginalDataElements;
import ap.sdk.message.PosDataCode;
import ap.sdk.message.PosEntryMode;
import ap.sdk.message.ProcessingCode;
import ap.sdk.message.iso8583.XFieldError;
import ap.sdk.util.Action;
import ap.sdk.util.FormatData;
import ap.sdk.util.Translate;
import ap.sdk.util.Utility;
import com.termappiso.Config;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.HashMap;
import org.xlightweb.IHttpExchange;

/**
 *
 * @author Abhishek M
 */
public class ProcessMsgFromTM implements Runnable
{

    String data_from_tm;
    IHttpExchange ihe;

    /**
     *
     * @param event
     */
    public ProcessMsgFromTM(Object event)
    {
        XHttpServer.DataEvent de = (XHttpServer.DataEvent) event;

        this.data_from_tm = de.data;
        this.ihe = de.ihe;
    }

    /**
     *
     */
    @Override
    public void run()
    {
        try
        {
            processMessageFromTM();
        } catch (ParseException | XFieldError ex)
        {
            Logger.getLogger(ProcessMsgFromTM.class.getName()).
                    log(Level.SEVERE, null, ex);
        }
    }

    /**
     *
     * @throws XFieldError
     * @throws ParseException
     */
    private void processMessageFromTM() throws XFieldError, ParseException
    {
        Iso8583JSON msg_from_tm;

        msg_from_tm = new Iso8583JSON();
        msg_from_tm.parseMsg(data_from_tm);

        Config.logger.debug("Msg from TM\n" + msg_from_tm.dumpMsg());

        String msg_type = msg_from_tm.getMsgType();
        Action action;

        switch (msg_type)
        {
            // Request from TM
            case Iso8583JSON.MsgTypeStr._0100_AUTH_REQ:
                action = process0100MsgFromTM(msg_from_tm);
                break;
            case Iso8583JSON.MsgTypeStr._0200_TRAN_REQ:
                action = process0200MsgFromTM(msg_from_tm);
                break;
            case Iso8583JSON.MsgTypeStr._0220_TRAN_ADV:
                action = process0220MsgFromTM(msg_from_tm);
                break;
            case Iso8583JSON.MsgTypeStr._0320_ACQUIRER_FILE_UPDATE_ADV:
                action = process0320MsgFromTM(msg_from_tm);
                break;
            case Iso8583JSON.MsgTypeStr._0420_ACQUIRER_REV_ADV:
                action = process0420MsgFromTM(msg_from_tm);
                break;
            case Iso8583JSON.MsgTypeStr._0520_ACQUIRER_RECONCILE_ADV:
                action = process0520MsgFromTM(msg_from_tm);
                break;
            case Iso8583JSON.MsgTypeStr._0800_NWRK_MNG_REQ:
                action = process0800Msg(msg_from_tm);
                break;

            default:
                action = processRejectFromTM(msg_from_tm);
                break;
        }
    }

    /**
     *
     * @param msg
     * @return
     */
    private Action process0100MsgFromTM(Iso8583JSON msg) throws XFieldError,
            ParseException
    {
        return constructFinMsgFromTM(msg);
    }

    /**
     *
     * @param msg
     * @return
     */
    private Action process0200MsgFromTM(Iso8583JSON msg) throws XFieldError,
            ParseException
    {
        return constructFinMsgFromTM(msg);
    }

    /**
     *
     * @param msg
     * @return
     * @throws XFieldError
     * @throws ParseException
     */
    private Action constructFinMsgFromTM(Iso8583JSON msg) throws XFieldError,
            ParseException
    {
        Iso8583PostilionTermApp msg_to_remote = new Iso8583PostilionTermApp();

        String msgtype = msg.getMsgType();

        msg_to_remote.setTPDU("B");

        switch (msgtype)
        {
            case Iso8583JSON.MsgTypeStr._0100_AUTH_REQ:
                msg_to_remote.setMsgType("1100");
                break;
            case Iso8583JSON.MsgTypeStr._0200_TRAN_REQ:
                msg_to_remote.setMsgType("1200");
                break;
        }

        msg_to_remote.setField(Iso8583PostilionTermApp.Bit._003_PROCESSING_CODE,
                msg.getField(Iso8583JSON.Bit._003_PROCESSING_CODE));
        msg_to_remote.setField(Iso8583PostilionTermApp.Bit._004_AMOUNT_TRANSACTION,
                msg.getField(Iso8583JSON.Bit._004_AMOUNT_TRANSACTION));
        msg_to_remote.setField(Iso8583PostilionTermApp.Bit._007_TRANSMISSION_DATE_TIME,
                msg.getField(Iso8583JSON.Bit._007_TRANSMISSION_DATE_TIME));
        msg_to_remote.setField(Iso8583PostilionTermApp.Bit._011_SYSTEMS_TRACE_AUDIT_NR,
                msg.getField(Iso8583JSON.Bit._011_SYSTEMS_TRACE_AUDIT_NR));
        msg_to_remote.setField(Iso8583PostilionTermApp.RemoteBit._012_DATE_TIME_LOCAL,
                msg.getField(Iso8583JSON.Bit._073_DATE_ACTION).substring(2)
                + msg.getField(Iso8583JSON.Bit._012_TIME_LOCAL));
        msg_to_remote.setField(Iso8583PostilionTermApp.Bit._022_POS_ENTRY_MODE,
                getPOSDataCode(msg.getField(Iso8583JSON.Bit._022_POS_ENTRY_MODE)));
        msg_to_remote.setField(Iso8583PostilionTermApp.Bit._023_CARD_SEQ_NR,
                msg.getField(Iso8583JSON.Bit._023_CARD_SEQ_NR));
        msg_to_remote.setField(Iso8583PostilionTermApp.Bit._024_NETWORK_INTL_ID, "200");
        msg_to_remote.setField(Iso8583PostilionTermApp.RemoteBit._029_RECON_INDICATOR,
                Utility.resize(Long.toString(
                        Long.parseLong(msg.getExtendedField(Iso8583JSON.ExtendedBit._006_TERM_BATCH_NR))),
                        3, "0", false));

        String trk2 = msg.getField(Iso8583JSON.Bit._035_TRACK_2_DATA);
        trk2 = trk2.replaceAll("d", "=");
        trk2 = trk2.replaceAll("D", "=");
        msg_to_remote.setField(Iso8583PostilionTermApp.Bit._035_TRACK_2_DATA, trk2);

        msg_to_remote.setField(Iso8583PostilionTermApp.Bit._041_CARD_ACCEPTOR_TERM_ID,
                msg.getField(Iso8583JSON.Bit._041_CARD_ACCEPTOR_TERM_ID));
        msg_to_remote.setField(Iso8583PostilionTermApp.Bit._042_CARD_ACCEPTOR_ID_CODE,
                msg.getField(Iso8583JSON.Bit._042_CARD_ACCEPTOR_ID_CODE));

        // Construct Field 48
        String f048 = constructField048(msg);

        msg_to_remote.setField(Iso8583PostilionTermApp.RemoteBit._048_ADDITIONAL_DATA, f048);

        msg_to_remote.setField(Iso8583PostilionTermApp.Bit._049_CURRENCY_CODE_TRAN,
                msg.getField(Iso8583JSON.Bit._049_CURRENCY_CODE_TRAN));

        if (msg.isFieldSet(Iso8583JSON.Bit._052_PIN_DATA))
        {
            msg_to_remote.setField(Iso8583PostilionTermApp.Bit._052_PIN_DATA,
                    Translate.fromHexToBin(msg.getField(Iso8583JSON.Bit._052_PIN_DATA)));

            String security_info = Translate.fromHexToBin(Utility.resize(msg.getField(Iso8583JSON.Bit._053_SECURITY_INFO), 96, "0", true));

            msg_to_remote.setField(Iso8583PostilionTermApp.Bit._053_SECURITY_INFO, security_info);
        }

        if (msg.isFieldSet(Iso8583JSON.Bit._055_EMV_DATA))
        {
            String emv_data = msg.getField(Iso8583JSON.Bit._055_EMV_DATA);

            Tlv tlv_req = new Tlv(Translate.fromHexToBin(emv_data));
            if (!msg.isFieldSet(Iso8583JSON.Bit._023_CARD_SEQ_NR))
            {
                String card_seq_nr = tlv_req.getField(Tag._5F34_APPLICATION_PAN_SEQ_NR);

                if (card_seq_nr != null && card_seq_nr.length() > 0)
                {
                    card_seq_nr = Utility.resize(Translate.fromBinToHex(card_seq_nr), 3, "0", false);
                    msg_to_remote.setField(Iso8583PostilionTermApp.Bit._023_CARD_SEQ_NR, card_seq_nr);
                }
            }

            String _9F12_APN = tlv_req.getField(Tag._9F12_APPLICATION_PREFERRED_NAME);
            if (null != _9F12_APN && _9F12_APN.length() > 0)
            {
                _9F12_APN = _9F12_APN.toLowerCase();
                Config.logger.debug("9F12 = " + _9F12_APN);
                ProcessingCode pcode = msg_to_remote.getProcessingCode();
                if (_9F12_APN.contains("maestro") || _9F12_APN.contains("chq")
                        || _9F12_APN.contains("cheque")
                        || _9F12_APN.contains("chequ"))
                {
                    pcode.setFromaccount(Iso8583PostilionTermApp.AccountType._20_CHECK);
                } else if (_9F12_APN.contains("savings"))
                {
                    pcode.setFromaccount(Iso8583PostilionTermApp.AccountType._10_SAVINGS);
                } else if (_9F12_APN.contains("debit"))
                {
                    pcode.setFromaccount(Iso8583PostilionTermApp.AccountType._20_CHECK);
                }
                msg_to_remote.setField(Iso8583PostilionTermApp.Bit._003_PROCESSING_CODE, pcode.getPCodeString());
            }

            String arqc = tlv_req.getField(Tag._9F26_APPLICATION_CRYPTOGRAM);
            String atc = tlv_req.getField(Tag._9F36_APPLICATION_TRANSACTION_COUNTER);

            if (null == arqc || null == atc)
            {
                Config.logger.warn("Processing transaction as fallback as ARQC (9F26) / ATC (9F36) is absent");
            } else
            {
                Tlv tlv_new_req = getTlv(tlv_req);

                String t_emv_data = Translate.fromBinToHex(Translate.getString(tlv_new_req.toMsg()));
                emv_data = "FF2081" + Utility.resize(Integer.toHexString(t_emv_data.length() / 2), 2, "0", false)
                        + t_emv_data;

                msg_to_remote.setField(Iso8583PostilionTermApp.Bit._055_EMV_DATA, Translate.fromHexToBin(emv_data));
            }
        }

        Config.logger.info("Request" + msg_to_remote.dumpMsg());
        Config.logger.trace("Request\n" + FormatData.hexdump(msg_to_remote.getMessage()));

        Iso8583PostilionTermApp msg_from_remote = new Iso8583PostilionTermApp();

        try
        {
            byte[] rsp_data = Utility.sendAndReceive2ByteHeaderData(Config.remoteIP,
                    Config.remotePort, msg_to_remote.getMessage(), 40000);

            Config.logger.trace("Response\n" + FormatData.hexdump(rsp_data));

            msg_from_remote.parseIso8583Msg(rsp_data);

            Config.logger.info("Message from Remote\n" + msg_from_remote.dumpMsg());

            String remote_rsp_code = msg_from_remote.getField(Iso8583PostilionTermApp.RemoteBit._039_RSP_CODE);

            if (Config.rspCodes.containsKey(remote_rsp_code))
            {
                msg_from_remote.setField(Iso8583PostilionTermApp.RemoteBit._039_RSP_CODE,
                        Config.rspCodes.get(remote_rsp_code));
            } else
            {
                msg_from_remote.setField(Iso8583PostilionTermApp.RemoteBit._039_RSP_CODE,
                        Iso8583JSON.RspCode._05_DO_NOT_HONOUR);
            }
        } catch (SocketTimeoutException ex)
        {
            Config.logger.error(ex);

            msg_from_remote.setField(Iso8583PostilionTermApp.Bit._037_RETRIEVAL_REF_NR,
                    msg.getField(Iso8583JSON.Bit._037_RETRIEVAL_REF_NR));
            msg_from_remote.setField(Iso8583PostilionTermApp.Bit._039_RSP_CODE, "91");
        } catch (IOException ex)
        {
            Config.logger.error(ex);

            msg_from_remote.setField(Iso8583PostilionTermApp.Bit._037_RETRIEVAL_REF_NR,
                    msg.getField(Iso8583JSON.Bit._037_RETRIEVAL_REF_NR));
            msg_from_remote.setField(Iso8583PostilionTermApp.Bit._039_RSP_CODE, "96");
        } catch (Exception ex)
        {
            Config.logger.error(ex);

            msg_from_remote.setField(Iso8583PostilionTermApp.Bit._037_RETRIEVAL_REF_NR,
                    msg.getField(Iso8583JSON.Bit._037_RETRIEVAL_REF_NR));
            msg_from_remote.setField(Iso8583PostilionTermApp.Bit._039_RSP_CODE, "96");
        }

        msg.removeField(Iso8583JSON.Bit._035_TRACK_2_DATA);
        msg.removeField(Iso8583JSON.Bit._045_TRACK_1_DATA);
        msg.removeField(Iso8583JSON.Bit._052_PIN_DATA);
        msg.removeField(Iso8583JSON.Bit._053_SECURITY_INFO);

        msg.setRspMsgType();
        msg.setField(Iso8583JSON.Bit._037_RETRIEVAL_REF_NR,
                msg_from_remote.getField(Iso8583PostilionTermApp.Bit._037_RETRIEVAL_REF_NR));
        msg.setField(Iso8583JSON.Bit._038_AUTH_ID_RSP,
                msg_from_remote.getField(Iso8583PostilionTermApp.Bit._038_AUTH_ID_RSP));
        msg.setField(Iso8583JSON.Bit._039_RSP_CODE,
                msg_from_remote.getField(Iso8583PostilionTermApp.Bit._039_RSP_CODE));

        if (msg_from_remote.isFieldSet(Iso8583PostilionTermApp.RemoteBit._055_EMV_DATA))
        {
            String rsp_emv_data;

            rsp_emv_data = msg_from_remote.getField(Iso8583PostilionTermApp.RemoteBit._055_EMV_DATA);

            if (rsp_emv_data.length() > 0)
            {
                rsp_emv_data = Translate.fromBinToHex(rsp_emv_data);
                rsp_emv_data = rsp_emv_data.substring(6);

                if (rsp_emv_data.startsWith("82"))
                {
                    rsp_emv_data = rsp_emv_data.substring(2);
                }

                msg.setField(Iso8583JSON.Bit._055_EMV_DATA, rsp_emv_data);
            }
        }

        Config.logger.debug("Message to TM\n" + msg.dumpMsg());
        Config.sendIHEResponse(ihe, msg.toMsg());

        return new Action();
    }

    /**
     *
     * @param msg
     * @return
     * @throws XFieldError
     */
    private Action process0220MsgFromTM(Iso8583JSON msg) throws XFieldError
    {
        msg.setRspMsgType();
        msg.setField(Iso8583JSON.Bit._039_RSP_CODE, Iso8583JSON.RspCode._00_SUCCESSFUL);

        Config.logger.debug("Response" + msg.dumpMsg());

        Config.sendIHEResponse(ihe, msg.toMsg());

        return new Action();
    }

    /**
     *
     * @param msg
     * @return
     * @throws XFieldError
     */
    private Action process0420MsgFromTM(Iso8583JSON msg) throws XFieldError, ParseException
    {
        Iso8583PostilionTermApp msg_to_remote = new Iso8583PostilionTermApp();

        msg_to_remote.setTPDU("B");

        msg_to_remote.setMsgType("1420");

        msg_to_remote.setField(Iso8583PostilionTermApp.Bit._003_PROCESSING_CODE,
                msg.getField(Iso8583JSON.Bit._003_PROCESSING_CODE));
        msg_to_remote.setField(Iso8583PostilionTermApp.Bit._004_AMOUNT_TRANSACTION,
                msg.getField(Iso8583JSON.Bit._004_AMOUNT_TRANSACTION));
        msg_to_remote.setField(Iso8583PostilionTermApp.Bit._007_TRANSMISSION_DATE_TIME,
                msg.getField(Iso8583JSON.Bit._007_TRANSMISSION_DATE_TIME));
        msg_to_remote.setField(Iso8583PostilionTermApp.Bit._011_SYSTEMS_TRACE_AUDIT_NR,
                msg.getField(Iso8583JSON.Bit._011_SYSTEMS_TRACE_AUDIT_NR));
        msg_to_remote.setField(Iso8583PostilionTermApp.RemoteBit._012_DATE_TIME_LOCAL,
                Utility.getDate().substring(2) + Utility.getTime());
        msg_to_remote.setField(Iso8583PostilionTermApp.Bit._022_POS_ENTRY_MODE,
                getPOSDataCode(msg.getField(Iso8583JSON.Bit._022_POS_ENTRY_MODE)));
        msg_to_remote.setField(Iso8583PostilionTermApp.Bit._023_CARD_SEQ_NR,
                msg.getField(Iso8583JSON.Bit._023_CARD_SEQ_NR));
        msg_to_remote.setField(Iso8583PostilionTermApp.Bit._024_NETWORK_INTL_ID, "200");
        msg_to_remote.setField(Iso8583PostilionTermApp.RemoteBit._029_RECON_INDICATOR,
                Utility.resize(Long.toString(
                        Long.parseLong(
                                msg.getExtendedField(Iso8583JSON.ExtendedBit._006_TERM_BATCH_NR))),
                        3, "0", false));

        if (msg.isFieldSet(Iso8583JSON.Bit._035_TRACK_2_DATA))
        {
            String trk2 = msg.getField(Iso8583JSON.Bit._035_TRACK_2_DATA);
            trk2 = trk2.replaceAll("d", "=");
            trk2 = trk2.replaceAll("D", "=");
            msg_to_remote.setField(Iso8583PostilionTermApp.Bit._035_TRACK_2_DATA, trk2);
        }

        msg_to_remote = constructRespCodeForReversal(msg, msg_to_remote);

        msg_to_remote.setField(Iso8583PostilionTermApp.Bit._041_CARD_ACCEPTOR_TERM_ID,
                msg.getField(Iso8583JSON.Bit._041_CARD_ACCEPTOR_TERM_ID));
        msg_to_remote.setField(Iso8583PostilionTermApp.Bit._042_CARD_ACCEPTOR_ID_CODE,
                msg.getField(Iso8583JSON.Bit._042_CARD_ACCEPTOR_ID_CODE));

        // Construct Field 48
        String f048 = constructField048(msg);

        msg_to_remote.setField(Iso8583PostilionTermApp.RemoteBit._048_ADDITIONAL_DATA, f048);

        msg_to_remote.setField(Iso8583PostilionTermApp.Bit._049_CURRENCY_CODE_TRAN,
                msg.getField(Iso8583JSON.Bit._049_CURRENCY_CODE_TRAN));

        if (msg.isFieldSet(Iso8583JSON.Bit._055_EMV_DATA))
        {
            String emv_data = msg.getField(Iso8583JSON.Bit._055_EMV_DATA);

            Tlv tlv_req = new Tlv(Translate.fromHexToBin(emv_data));
            if (!msg.isFieldSet(Iso8583JSON.Bit._023_CARD_SEQ_NR))
            {
                String card_seq_nr = tlv_req.getField(Tag._5F34_APPLICATION_PAN_SEQ_NR);

                if (card_seq_nr != null && card_seq_nr.length() > 0)
                {
                    card_seq_nr = Utility.resize(Translate.fromBinToHex(card_seq_nr), 3, "0", false);
                    msg_to_remote.setField(Iso8583PostilionTermApp.Bit._023_CARD_SEQ_NR, card_seq_nr);
                }
            }

            Tlv tlv_new_req = getTlv(tlv_req);

            String t_emv_data = Translate.fromBinToHex(Translate.getString(tlv_new_req.toMsg()));
            emv_data = "FF2081" + Utility.resize(Integer.toHexString(t_emv_data.length() / 2), 2, "0", false)
                    + t_emv_data;

            msg_to_remote.setField(Iso8583PostilionTermApp.Bit._055_EMV_DATA, Translate.fromHexToBin(emv_data));
        }

        String f056;
        OriginalDataElements ode = new OriginalDataElements(msg.getField(Iso8583JSON.Bit._090_ORIGINAL_DATA_ELEMENTS));
        f056 = "1200" + ode.getStan() + ode.getDatetime() + Utility.resize("0", 11, "0", true);

        msg_to_remote.setField(56, f056);

        Config.logger.info("Request" + msg_to_remote.dumpMsg());
        Config.logger.trace("Request\n" + FormatData.hexdump(msg_to_remote.getMessage()));

        Iso8583PostilionTermApp msg_from_remote = new Iso8583PostilionTermApp();

        try
        {
            byte[] rsp_data = Utility.sendAndReceive2ByteHeaderData(Config.remoteIP,
                    Config.remotePort, msg_to_remote.getMessage(), 40000);

            Config.logger.trace("Response\n" + FormatData.hexdump(rsp_data));

            msg_from_remote.parseIso8583Msg(rsp_data);

            Config.logger.info("Message from Remote\n" + msg_from_remote.dumpMsg());

            String remote_rsp_code = msg_from_remote.getField(Iso8583PostilionTermApp.RemoteBit._039_RSP_CODE);

            if (Config.rspCodes.containsKey(remote_rsp_code))
            {
                msg_from_remote.setField(Iso8583PostilionTermApp.RemoteBit._039_RSP_CODE,
                        Config.rspCodes.get(remote_rsp_code));
            } else
            {
                msg_from_remote.setField(Iso8583PostilionTermApp.RemoteBit._039_RSP_CODE, "05");
            }
        } catch (SocketTimeoutException ex)
        {
            Config.logger.error(ex);

            msg_from_remote.setField(Iso8583PostilionTermApp.Bit._037_RETRIEVAL_REF_NR,
                    msg.getField(Iso8583JSON.Bit._037_RETRIEVAL_REF_NR));
            msg_from_remote.setField(Iso8583PostilionTermApp.Bit._039_RSP_CODE, "91");
        } catch (IOException ex)
        {
            Config.logger.error(ex);

            msg_from_remote.setField(Iso8583PostilionTermApp.Bit._037_RETRIEVAL_REF_NR,
                    msg.getField(Iso8583JSON.Bit._037_RETRIEVAL_REF_NR));
            msg_from_remote.setField(Iso8583PostilionTermApp.Bit._039_RSP_CODE, "96");
        } catch (Exception ex)
        {
            Config.logger.error(ex);

            msg_from_remote.setField(Iso8583PostilionTermApp.Bit._037_RETRIEVAL_REF_NR,
                    msg.getField(Iso8583JSON.Bit._037_RETRIEVAL_REF_NR));
            msg_from_remote.setField(Iso8583PostilionTermApp.Bit._039_RSP_CODE, "96");
        }

        msg.removeField(Iso8583JSON.Bit._035_TRACK_2_DATA);
        msg.removeField(Iso8583JSON.Bit._045_TRACK_1_DATA);
        msg.removeField(Iso8583JSON.Bit._052_PIN_DATA);
        msg.removeField(Iso8583JSON.Bit._053_SECURITY_INFO);

        msg.setRspMsgType();
        msg.setField(Iso8583JSON.Bit._037_RETRIEVAL_REF_NR,
                msg_from_remote.getField(Iso8583PostilionTermApp.Bit._037_RETRIEVAL_REF_NR));
        msg.setField(Iso8583JSON.Bit._038_AUTH_ID_RSP,
                msg_from_remote.getField(Iso8583PostilionTermApp.Bit._038_AUTH_ID_RSP));
        msg.setField(Iso8583JSON.Bit._039_RSP_CODE,
                msg_from_remote.getField(Iso8583PostilionTermApp.Bit._039_RSP_CODE));

        if (msg_from_remote.isFieldSet(Iso8583PostilionTermApp.RemoteBit._055_EMV_DATA))
        {
            String rsp_emv_data;

            rsp_emv_data = msg_from_remote.getField(Iso8583PostilionTermApp.RemoteBit._055_EMV_DATA);

            if (rsp_emv_data.length() > 0)
            {
                rsp_emv_data = Translate.fromBinToHex(rsp_emv_data);
                rsp_emv_data = rsp_emv_data.substring(6);

                msg.setField(Iso8583JSON.Bit._055_EMV_DATA, rsp_emv_data);
            }
        }

        Config.logger.debug("Message to TM\n" + msg.dumpMsg());
        Config.sendIHEResponse(ihe, msg.toMsg());

        return new Action();
    }

    /**
     *
     * @param msg
     * @return
     * @throws XFieldError
     */
    private Action process0520MsgFromTM(Iso8583JSON msg) throws XFieldError
    {
        msg.setRspMsgType();

        msg.setField(Iso8583JSON.Bit._039_RSP_CODE, Iso8583JSON.RspCode._00_SUCCESSFUL);

        Config.logger.debug("Response" + msg.dumpMsg());

        Config.sendIHEResponse(ihe, msg.toMsg());

        return new Action();
    }

    /**
     *
     * @param msg
     * @return
     */
    private Action processRejectFromTM(Iso8583JSON msg)
    {
        msg.setRspMsgType();
        msg.setField(Iso8583JSON.Bit._039_RSP_CODE, Iso8583JSON.RspCode._12_INVALID_TRAN);

        Config.logger.warn("Response" + msg.dumpMsg());

        Config.sendIHEResponse(ihe, msg.toMsg());

        return new Action();
    }

    /**
     *
     * @param msg_from_tm
     * @return
     */
    private Action process0320MsgFromTM(Iso8583JSON msg) throws XFieldError
    {
        msg.setRspMsgType();

        msg.setField(Iso8583JSON.Bit._039_RSP_CODE, Iso8583JSON.RspCode._00_SUCCESSFUL);

        Config.logger.debug("Response" + msg.dumpMsg());

        return new Action(null, msg.toMsg().getBytes());
    }

    /**
     *
     * @param msg
     * @param msg_to_remote
     * @return
     */
    private Iso8583PostilionTermApp constructRespCodeForReversal(Iso8583JSON msg, Iso8583PostilionTermApp msg_to_remote)
    {
        String resp_code = msg.getField(Iso8583JSON.Bit._039_RSP_CODE);

        // Customer Cancellation - VOID Transaction
        switch (resp_code)
        {
            case Iso8583JSON.RspCode._17_CUSTOMER_CANCELLATION:
                msg_to_remote.setField(Iso8583PostilionTermApp.Bit._039_RSP_CODE, "000");

                return msg_to_remote;
            case Iso8583JSON.RspCode._68_RESPONSE_RECEIVED_TOO_LATE:
                msg_to_remote.setField(Iso8583PostilionTermApp.Bit._039_RSP_CODE, "911");

                return msg_to_remote;
            default:
                resp_code = "000";
                break;
        }

        // For Acquirer Timeout
        String f060 = msg.getField(Iso8583JSON.Bit._060_ADVICE_REASON_CODE);
        if (f060 != null && f060.equals(Iso8583JSON.AdviceReasonCode._910_0011_TM_TO))
        {
            msg_to_remote.setField(Iso8583PostilionTermApp.Bit._039_RSP_CODE, "911");

            return msg_to_remote;
        }

        msg_to_remote.setField(Iso8583PostilionTermApp.Bit._039_RSP_CODE, resp_code);

        return msg_to_remote;
    }

    /**
     *
     * @param data_from_remote
     * @return
     */
    private Tlv getTlv(Tlv data_from_remote)
    {
        Tlv data_to_tm = new Tlv();

        data_to_tm.putField(Tag._9F02_AMOUNT_AUTHORIZED_NUMERIC,
                data_from_remote.getField(Tag._9F02_AMOUNT_AUTHORIZED_NUMERIC));
        data_to_tm.putField(Tag._9F03_AMOUNT_OTHER_NUMERIC,
                data_from_remote.getField(Tag._9F03_AMOUNT_OTHER_NUMERIC));
        data_to_tm.putField(Tag._4F_APPLICATION_IDENTIFIER,
                data_from_remote.getField(Tag._4F_APPLICATION_IDENTIFIER));
        data_to_tm.putField(Tag._82_APPLICATION_INTERCHANGE_PROFILE,
                data_from_remote.getField(Tag._82_APPLICATION_INTERCHANGE_PROFILE));
        data_to_tm.putField(Tag._9F36_APPLICATION_TRANSACTION_COUNTER,
                data_from_remote.getField(Tag._9F36_APPLICATION_TRANSACTION_COUNTER));
        data_to_tm.putField(Tag._9F07_APPLICATION_USAGE_CONTROL,
                data_from_remote.getField(Tag._9F07_APPLICATION_USAGE_CONTROL));
        data_to_tm.putField(Tag._9F26_APPLICATION_CRYPTOGRAM,
                data_from_remote.getField(Tag._9F26_APPLICATION_CRYPTOGRAM));
        data_to_tm.putField(Tag._9F27_CRYPTOGRAM_INFORMATION_DATA,
                data_from_remote.getField(Tag._9F27_CRYPTOGRAM_INFORMATION_DATA));
        data_to_tm.putField(Tag._8E_CVM_LIST,
                data_from_remote.getField(Tag._8E_CVM_LIST));
        data_to_tm.putField(Tag._9F34_CVM_RESULTS,
                data_from_remote.getField(Tag._9F34_CVM_RESULTS));
        data_to_tm.putField(Tag._9F1E_IFD_SERIAL_NUMBER,
                data_from_remote.getField(Tag._9F1E_IFD_SERIAL_NUMBER));
        data_to_tm.putField(Tag._9F0D_ISSUER_ACTION_CODE_DEFAULT,
                data_from_remote.getField(Tag._9F0D_ISSUER_ACTION_CODE_DEFAULT));
        data_to_tm.putField(Tag._9F0E_ISSUER_ACTION_CODE_DENIAL,
                data_from_remote.getField(Tag._9F0E_ISSUER_ACTION_CODE_DENIAL));
        data_to_tm.putField(Tag._9F0F_ISSUER_ACTION_CODE_ONLINE,
                data_from_remote.getField(Tag._9F0F_ISSUER_ACTION_CODE_ONLINE));
        data_to_tm.putField(Tag._9F10_ISSUER_APPLICATION_DATA,
                data_from_remote.getField(Tag._9F10_ISSUER_APPLICATION_DATA));
        data_to_tm.putField(Tag._9F09_TERM_APPLICATION_VERSION_NR,
                data_from_remote.getField(Tag._9F09_TERM_APPLICATION_VERSION_NR));
        data_to_tm.putField(Tag._9F33_TERMINAL_CAPABILITIES,
                data_from_remote.getField(Tag._9F33_TERMINAL_CAPABILITIES));
        data_to_tm.putField(Tag._9F1A_TERMINAL_COUNTRY_CODE,
                data_from_remote.getField(Tag._9F1A_TERMINAL_COUNTRY_CODE));
        data_to_tm.putField(Tag._9F35_TERMINAL_TYPE,
                data_from_remote.getField(Tag._9F35_TERMINAL_TYPE));
        data_to_tm.putField(Tag._95_TERMINAL_VERIFICATION_RESULTS,
                data_from_remote.getField(Tag._95_TERMINAL_VERIFICATION_RESULTS));
        data_to_tm.putField(Tag._9F53_TRANSACTION_CATEGORY_CODE,
                data_from_remote.getField(Tag._9F53_TRANSACTION_CATEGORY_CODE));
        data_to_tm.putField(Tag._5F2A_TRANSACTION_CURRENCY_CODE,
                data_from_remote.getField(Tag._5F2A_TRANSACTION_CURRENCY_CODE));
        data_to_tm.putField(Tag._9A_TRANSACTION_DATE,
                data_from_remote.getField(Tag._9A_TRANSACTION_DATE));
        data_to_tm.putField(Tag._9F41_TRANSACTION_SEQUENCE_COUNTER,
                data_from_remote.getField(Tag._9F41_TRANSACTION_SEQUENCE_COUNTER));
        data_to_tm.putField(Tag._9C_TRANSACTION_TYPE,
                data_from_remote.getField(Tag._9C_TRANSACTION_TYPE));
        data_to_tm.putField(Tag._9F37_UNPREDICTABLE_NUMBER,
                data_from_remote.getField(Tag._9F37_UNPREDICTABLE_NUMBER));
        data_to_tm.putField(Tag._9F6E_FORM_FACTOR_INDICATOR,
                data_from_remote.getField(Tag._9F6E_FORM_FACTOR_INDICATOR));
        data_to_tm.putField(Tag._9F7C_CUSTOMER_EXCLUSIVE_DATA,
                data_from_remote.getField(Tag._9F7C_CUSTOMER_EXCLUSIVE_DATA));

        return data_to_tm;
    }

    /**
     *
     * @param msg
     * @return
     */
    private boolean isTimeoutReversal(Iso8583JSON msg)
    {
        String f060;

        if (msg.isFieldSet(Iso8583JSON.Bit._060_ADVICE_REASON_CODE))
        {
            f060 = msg.getField(Iso8583JSON.Bit._060_ADVICE_REASON_CODE);
            return f060 != null && f060.trim().equals("9100011");
        } else
        {
            return false;
        }
    }

    /**
     *
     * @param msg
     * @return
     */
    private boolean isFallback(Iso8583JSON msg)
    {
        String servicecode = "000";
        boolean flg;

        if (msg.isFieldSet(Iso8583JSON.Bit._040_SERVICE_RESTRICTION_CODE))
        {
            servicecode = msg.getField(Iso8583JSON.Bit._040_SERVICE_RESTRICTION_CODE);
        }

        if (!msg.isFieldSet(Iso8583JSON.Bit._055_EMV_DATA))
        {
            flg = servicecode.startsWith("2") || servicecode.startsWith("6");
        } else
        {
            flg = false;
        }

        return flg;
    }

    /**
     *
     * @param msg_from_tm
     * @return
     * @throws ParseException
     * @throws XFieldError
     */
    private Action process0800Msg(Iso8583JSON msg_from_tm) throws ParseException, XFieldError
    {
        Iso8583PostilionTermApp msg_to_remote = new Iso8583PostilionTermApp();

        String stan = Utility.getDateTime().substring(4);
        String datetime = Utility.getGMTDateTime(Utility.getDateTime());

        msg_to_remote.setMsgType(Iso8583PostilionTermApp.MsgTypeStr._0800_NWRK_MNG_REQ);
        msg_to_remote.setField(Iso8583PostilionTermApp.Bit._007_TRANSMISSION_DATE_TIME, datetime);
        msg_to_remote.setField(Iso8583PostilionTermApp.Bit._011_SYSTEMS_TRACE_AUDIT_NR, stan);
        msg_to_remote.setField(Iso8583PostilionTermApp.Bit._012_TIME_LOCAL, stan);

        msg_to_remote.setField(Iso8583PostilionTermApp.Bit._070_NETWORK_MNG_INFO_CODE,
                msg_from_tm.getField(Iso8583JSON.Bit._070_NETWORK_MNG_INFO_CODE));

        Config.logger.info("Network Msg\n" + msg_to_remote.dumpMsg());

        msg_from_tm.setRspMsgType();
        msg_from_tm.setField(Iso8583JSON.Bit._039_RSP_CODE, "00");

        Config.sendIHEResponse(ihe, msg_from_tm.toMsg());

        return new Action();
    }

    /**
     *
     * @param msg
     * @return
     */
    private String constructField048(Iso8583JSON msg) throws XFieldError
    {
        AdditionalData ad = new AdditionalData();
        String posdata;
        String transactionId;
        String operatorid = "00000";

        transactionId = msg.getField(Iso8583JSON.Bit._062_TRANS_ID);

        if (transactionId != null)
        {
            if (transactionId.trim().length() > 6)
            {
                transactionId = transactionId.substring(transactionId.trim().length() - 6);
            }
        } else
        {
            transactionId = "0";
        }

        transactionId = Utility.resize(transactionId, 6, "0", false);
        posdata = msg.getField(Iso8583JSON.Bit._041_CARD_ACCEPTOR_TERM_ID) + transactionId + operatorid;

        ad.setField(1, posdata);

        // Structured Data
        String key;
        String psd = "";
        PostilionKeyValuePair pkvp = new PostilionKeyValuePair();

        KeyValuePair kvp = new KeyValuePair();
        HashMap<String, String> hm_kvp = kvp.formHM(msg.getField(Iso8583JSON.Bit._120_TRAN_DATA_REQ));

        for (int i = 0; i < 10; i++)
        {
            key = "AddlNote" + i;

            if (hm_kvp.containsKey(key))
            {
                pkvp.put(key, hm_kvp.get(key));
            }
        }

        if (!pkvp.getKVPHM().isEmpty())
        {
            psd = pkvp.formMsg();

            ad.setField(16, psd);
        }

        return Translate.getString(ad.getMessage());
    }

    /**
     *
     * @param posentrymode
     * @return
     */
    private String getPOSDataCode(String posentrymode)
    {
        posentrymode = posentrymode.substring(0, 2);

        PosDataCode pdc = new PosDataCode("51010151414C127");

        switch (posentrymode)
        {
            case PosEntryMode.PanEntryMode._01_MANUAL:
                pdc.putCardDataInputMode(PosDataCode.CardDataInputMode._1_MANUAL);
                break;

            case PosEntryMode.PanEntryMode._05_INTEGRATED_CIRCUIT_CARD:
                pdc.putCardDataInputMode(PosDataCode.CardDataInputMode._5_ICC);
                break;

            case PosEntryMode.PanEntryMode._07_CONTACTLESS_INTEGRATED_CIRCUIT_CARD:
                pdc.putCardDataInputMode(PosDataCode.CardDataInputMode._7_CONTACTLESS_ICC);
                break;

            case PosEntryMode.PanEntryMode._91_CONTACTLESS_MAGNETIC_STRIPE:
                pdc.putCardDataInputMode(PosDataCode.CardDataInputMode._8_CONTACTLESS_MAGSTRIPE);
                break;

            case PosEntryMode.PanEntryMode._90_MAGNETIC_STRIPE:
                pdc.putCardDataInputMode(PosDataCode.CardDataInputMode._2_MAGSTRIPE);
                break;
        }

        return pdc.toString().trim();
    }
}
