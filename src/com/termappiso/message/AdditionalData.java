package com.termappiso.message;

import ap.sdk.message.Iso8583JSON;
import ap.sdk.message.iso8583.DataFormatter;
import ap.sdk.message.iso8583.Field;
import ap.sdk.message.iso8583.LengthFormatter;
import ap.sdk.message.iso8583.XFieldError;
import ap.sdk.util.FormatData;
import ap.sdk.util.Translate;
import ap.sdk.util.Utility;
import java.util.BitSet;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 * @author abhishek.m
 */
public class AdditionalData extends Iso8583JSON
{

    ConcurrentHashMap<Integer, Field> fieldmap_048;

    public AdditionalData()
    {
        fieldmap_048 = new ConcurrentHashMap<>(16);

        fieldmap_048.put(0, new Field(new DataFormatter(DataFormatter.DataType._HEX,
                "", false, true, 4),
                new LengthFormatter(LengthFormatter.LengthType._FIXED_LEN,
                        LengthFormatter.LengthFormat._NONE, false,
                        4)));
        fieldmap_048.put(1,
                new Field(new DataFormatter(DataFormatter.DataType._HEX,
                        "0", false, false, 19),
                        new LengthFormatter(LengthFormatter.LengthType._FIXED_LEN,
                                LengthFormatter.LengthFormat._NONE, false,
                                19)));
        fieldmap_048.put(2,
                new Field(new DataFormatter(DataFormatter.DataType._HEX,
                        "0", false, false, 2),
                        new LengthFormatter(LengthFormatter.LengthType._FIXED_LEN,
                                LengthFormatter.LengthFormat._BCD, false,
                                2)));
        fieldmap_048.put(3,
                new Field(new DataFormatter(DataFormatter.DataType._HEX,
                        "0", false, false, 4),
                        new LengthFormatter(LengthFormatter.LengthType._FIXED_LEN,
                                LengthFormatter.LengthFormat._BCD, false,
                                4)));
        fieldmap_048.put(4,
                new Field(new DataFormatter(DataFormatter.DataType._HEX,
                        "0", false, false, 4),
                        new LengthFormatter(LengthFormatter.LengthType._FIXED_LEN,
                                LengthFormatter.LengthFormat._BCD, false,
                                4)));
        fieldmap_048.put(5,
                new Field(new DataFormatter(DataFormatter.DataType._HEX,
                        "0", false, false, 255),
                        new LengthFormatter(LengthFormatter.LengthType._LLLVAR,
                                LengthFormatter.LengthFormat._HEX, false,
                                255)));
        fieldmap_048.put(6,
                new Field(new DataFormatter(DataFormatter.DataType._HEX,
                        "0", false, false, 999),
                        new LengthFormatter(LengthFormatter.LengthType._LLLVAR,
                                LengthFormatter.LengthFormat._HEX, false,
                                999)));
        fieldmap_048.put(7,
                new Field(new DataFormatter(DataFormatter.DataType._HEX,
                        "0", false, false, 48),
                        new LengthFormatter(LengthFormatter.LengthType._FIXED_LEN,
                                LengthFormatter.LengthFormat._BCD, false,
                                48)));
        fieldmap_048.put(8,
                new Field(new DataFormatter(DataFormatter.DataType._HEX,
                        "0", false, false, 50),
                        new LengthFormatter(LengthFormatter.LengthType._LLVAR,
                                LengthFormatter.LengthFormat._HEX, false,
                                50)));
        fieldmap_048.put(9,
                new Field(new DataFormatter(DataFormatter.DataType._HEX,
                        "0", false, false, 29),
                        new LengthFormatter(LengthFormatter.LengthType._FIXED_LEN,
                                LengthFormatter.LengthFormat._BCD, false,
                                29)));
        fieldmap_048.put(10,
                new Field(new DataFormatter(DataFormatter.DataType._HEX,
                        "0", false, false, 1),
                        new LengthFormatter(LengthFormatter.LengthType._FIXED_LEN,
                                LengthFormatter.LengthFormat._BCD, false,
                                1)));
        fieldmap_048.put(11,
                new Field(new DataFormatter(DataFormatter.DataType._HEX,
                        "0", false, false, 1),
                        new LengthFormatter(LengthFormatter.LengthType._FIXED_LEN,
                                LengthFormatter.LengthFormat._BCD, false,
                                1)));
        fieldmap_048.put(12,
                new Field(new DataFormatter(DataFormatter.DataType._HEX,
                        "0", false, false, 999),
                        new LengthFormatter(LengthFormatter.LengthType._LLLVAR,
                                LengthFormatter.LengthFormat._HEX, false,
                                999)));
        fieldmap_048.put(13,
                new Field(new DataFormatter(DataFormatter.DataType._HEX,
                        "0", false, false, 31),
                        new LengthFormatter(LengthFormatter.LengthType._FIXED_LEN,
                                LengthFormatter.LengthFormat._BCD, false,
                                31)));
        fieldmap_048.put(14,
                new Field(new DataFormatter(DataFormatter.DataType._HEX,
                        "0", false, false, 253),
                        new LengthFormatter(LengthFormatter.LengthType._FIXED_LEN,
                                LengthFormatter.LengthFormat._BCD, false,
                                253)));
        fieldmap_048.put(15,
                new Field(new DataFormatter(DataFormatter.DataType._HEX,
                        "0", false, false, 28),
                        new LengthFormatter(LengthFormatter.LengthType._LLVAR,
                                LengthFormatter.LengthFormat._HEX, false,
                                28)));
        fieldmap_048.put(16,
                new Field(new DataFormatter(DataFormatter.DataType._HEX,
                        "0", false, false, 9999),
                        new LengthFormatter(LengthFormatter.LengthType._LLLLVAR,
                                LengthFormatter.LengthFormat._HEX, false,
                                9999)));
    }

    /**
     *
     * @return @throws XFieldError
     */
    protected byte[] getMessage() throws XFieldError
    {
        Field f;
        String fieldvalue;
        byte[] msg;
        byte[] temp;
        int totallen = 0;
        int pos = 0;
        BitSet bitmap = new BitSet();

        for (int i = 0; i <= 16; i++)
        {
            if (i != 0)
            {
                fieldvalue = getField(i);
                if (fieldvalue != null && fieldvalue.length() > 0)
                {
                    f = fieldmap_048.get(i);

                    f.setContent(fieldvalue);
                    temp = f.getFormattedContent();

                    totallen = totallen + temp.length;

                    bitmap.set(i);

                    if (i > 64)
                    {
                        bitmap.set(1);
                    }
                }
            }
        }

        byte[] byte_bitmap = FormatData.bitSet2byte(bitmap);
        totallen = totallen + 2;//byte_bitmap.length;

        String header = Translate.fromHexToBin("f0" + Utility.resize(Integer.toHexString(totallen), 4, "0", false));

        msg = new byte[totallen + 3];

        System.arraycopy(Translate.getData(header), 0, msg, 0, 3);

        pos = pos + 3;

        for (int i = 0; i <= 16; i++)
        {
            if (i != 0)
            {
                fieldvalue = getField(i);

                if (fieldvalue != null && fieldvalue.length() > 0)
                {
                    f = fieldmap_048.get(i);

                    f.setContent(fieldvalue);
                    temp = f.getFormattedContent();
                    System.arraycopy(temp, 0, msg, pos, temp.length);
                    pos = pos + temp.length;
                }
            } else
            {
                System.arraycopy(byte_bitmap, 0, msg, pos, 2);// byte_bitmap.length);
                pos = pos + 2;//byte_bitmap.length;
            }
        }

        return msg;
    }

    /**
     *
     * @param data
     * @throws Exception
     */
    public void parseMsg(byte[] data) throws Exception
    {
        Field f;
        int flen;
        byte[] temp = null;
        byte[] temp_len;
        int pos = 3;
        int len_of_len;

        byte start = data[0];
        byte[] header;

        // read the Bitmap
        f = fieldmap_048.get(3);
        BitSet bitmap = FormatData.byte2BitSet(data, pos, 16);
        if (bitmap.get(1))
        {
            flen = 2;
        } else
        {
            flen = 2;
        }

        if (f.getFieldDataType() == DataFormatter.DataType._BINARY)
        {
            flen = flen / 2;
        }

        pos = pos + flen;

        // process for the remaining fields
        for (int i = 1; i <= 16; i++)
        {
            if (bitmap.get(i))
            {
                f = fieldmap_048.get(i);
                if (f.getFieldLengthType()
                        == LengthFormatter.LengthType._FIXED_LEN)
                {
                    flen = f.getFieldDataParseMsgLen();

                    switch (f.getFieldDataType())
                    {
                        case DataFormatter.DataType._BINARY:
                            temp = new byte[flen];
                            System.arraycopy(data, pos, temp, 0, flen);
                            String temp_str = Translate.fromBinToHex(Translate.
                                    getString(temp));
                            int act_len = f.getFieldDataLength();
                            temp_str = Utility.removePadChars(temp_str,
                                    f.getDf().isPadRight(), act_len);
                            setField(i, temp_str);
                            break;
                        case DataFormatter.DataType._EBCDIC:
                            temp = new byte[flen];
                            System.arraycopy(data, pos, temp, 0, flen);
                            setField(i, Translate.fromEbcdicToAscii(Translate.
                                    getString(temp)));
                            break;
                        case DataFormatter.DataType._HEX:
                            temp = new byte[flen];
                            System.arraycopy(data, pos, temp, 0, flen);
                            setField(i, Translate.getString(temp));
                            break;
                        default:
                            break;
                    }
                    pos = pos + flen;
                } else
                {
                    flen = f.getFieldParseLength();
                    temp_len = new byte[flen];
                    System.arraycopy(data, pos, temp_len, 0, flen);

                    switch (f.getFieldLengthFormat())
                    {
                        case LengthFormatter.LengthFormat._BINARY:
                            len_of_len = Integer.parseInt(Translate.fromBinToHex(Translate.
                                    getString(temp_len)));
                            break;
                        case LengthFormatter.LengthFormat._EBCDIC:
                            len_of_len = Integer.parseInt(Translate.
                                    fromEbcdicToAscii(Translate.getString(temp_len)));
                            break;
                        case LengthFormatter.LengthFormat._HEX:
                            len_of_len = Integer.parseInt(Translate.getString(temp_len));
                            break;
                        case LengthFormatter.LengthFormat._BCD:
                            len_of_len = Integer.parseInt(Translate.fromBinToHex(Translate.
                                    getString(temp_len)), 16);
                            break;
                        default:
                            len_of_len = Integer.parseInt(Translate.getString(temp_len));
                            break;
                    }

                    pos = pos + flen;

                    switch (f.getFieldDataType())
                    {
                        case DataFormatter.DataType._BINARY:
                            int act_len = len_of_len;
                            if (Utility.isOdd(len_of_len))
                            {
                                len_of_len = (len_of_len + 1) / 2;
                            } else
                            {
                                len_of_len = len_of_len / 2;
                            }   temp = new byte[len_of_len];
                            System.arraycopy(data, pos, temp, 0, len_of_len);
                            String temp_str = Translate.fromBinToHex(Translate.
                                    getString(temp));
                            temp_str = Utility.removePadChars(temp_str, f.getDf().
                                    isPadRight(), act_len);
                            setField(i, temp_str);
                            break;
                        case DataFormatter.DataType._EBCDIC:
                            temp = new byte[len_of_len];
                            System.arraycopy(data, pos, temp, 0, len_of_len);
                            setField(i, Translate.fromEbcdicToAscii(Translate.
                                    getString(temp)));
                            break;
                        case DataFormatter.DataType._HEX:
                            temp = new byte[len_of_len];
                            System.arraycopy(data, pos, temp, 0, len_of_len);
                            setField(i, Translate.getString(temp));
                            break;
                        default:
                            break;
                    }
                    pos = pos + len_of_len;
                }
            }
        }
    }

    /**
     *
     * @return
     */
    @Override
    public String dumpMsg()
    {
        StringBuilder str = new StringBuilder("");
        String value;

        // print all the fields except Extended Field 123
        for (int i = 1; i <= 16; i++)
        {
            value = getField(i);

            if (value != null && value.trim().length() > 0)
            {
                str.append("\t\t[F").append(Utility.resize(Integer.
                        toString(i), 3, "0",
                        false)).append("] = [").append(getField(i)).
                        append("]\n");
            }
        }

        return str.toString();
    }

    /**
     *
     * @param args
     * @throws Exception
     */
//    public static void main(String[] args) throws Exception
//    {
//        AdditionalData c = new AdditionalData();
//
//        c.setField(1, "1234567800000155555");
//        c.setField(12, "16TrmTyp214mSwipe WisePad19TrmAppVer144.0A18TrmSerNo2141510010001607A18TrmFWVer2113.54.03140118EMVL2Ver136.013APN1014IMEI1014SCID1017Cashier18thabisos13TID1013MID10");
//        c.setField(16, "16TxnRef16ZipZap212TermSerialNo2141510010001607A14PPIS21614User18thabisos225Base24SaPOSBatchShiftData3105<Base24SaPOSBatchShiftData><BatchSeqNr>901</BatchSeqNr><BatchNr>157</BatchNr></Base24SaPOSBatchShiftData>");
//        System.out.println(FormatData.hexdump(c.getMessage()));
//
//        System.out.println(c.dumpMsg());
//        //c.parseMsg(Translate.getData(Translate.fromHexToBin("40000000000000000586096248638656")));
//
//        //System.out.println(c.getField(2));
//    }

    }
