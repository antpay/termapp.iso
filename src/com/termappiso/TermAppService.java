package com.termappiso;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.boris.winrun4j.AbstractService;
import org.boris.winrun4j.ServiceException;

/**
 *
 * @author abhishek.m
 */
public class TermAppService extends AbstractService
{

    /**
     *
     * @param strings
     * @return
     * @throws ServiceException
     */
    @Override
    public int serviceMain(String[] strings) throws ServiceException
    {
        TermAppISO ti = new TermAppISO(strings[0]);
        ti.startprocess();

        while (!shutdown)
        {
            try
            {
                Thread.sleep(5000);
            } catch (InterruptedException ex)
            {
                Logger.getLogger(TermAppISO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        ti.shutdown();
        
        System.exit(0);

        return 0;
    }

}
