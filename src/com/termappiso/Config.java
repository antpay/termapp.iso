package com.termappiso;

import ap.sdk.comms.XHttpServer;
import ap.sdk.jdbc.DBManager;
import ap.sdk.message.Iso8583JSON;
import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xlightweb.HttpResponse;
import org.xlightweb.IHttpExchange;

/**
 *
 * @author Abhishek M
 */
public class Config
{
    
    public String moduleName;
    public static DBManager dbm;
    public static Logger logger;
    public static XHttpServer tmServer;
    public static String _MODULE_NAME;
    
    public static boolean isRemoteConnected;
    
    public static String remoteIP;
    public static int remotePort;
    
    public static ConcurrentHashMap<String, String> rspCodes;

    /**
     *
     * @param moduleName
     */
    public static void initializeConfigurations(String moduleName)
    {
        _MODULE_NAME = moduleName;
        
        logger = LogManager.getLogger(moduleName);
        logger.info("Initializing module ... ");
        
        dbm = new DBManager();
        
        isRemoteConnected = false;
        
        rspCodes = new ConcurrentHashMap<>();
        
        loadResponseCodes();
    }

    /**
     *
     * @param ihe
     * @param body
     * @return
     */
    public static boolean sendIHEResponse(IHttpExchange ihe, String body)
    {
        boolean sent = false;
        
        try
        {
            if (ihe.getConnection().isOpen())
            {
                ihe.send(new HttpResponse(body));
                sent = true;
            } else
            {
                logger.error("Could not send response");
            }
        } catch (IOException ex)
        {
            logger.error("Error sending message #sendIHEResponse");
            logger.error(ex);
        }
        
        return sent;
    }

    /**
     *
     */
    protected static void loadResponseCodes()
    {
        rspCodes.put("000", Iso8583JSON.RspCode._00_SUCCESSFUL);
        rspCodes.put("107", Iso8583JSON.RspCode._01_REFER_TO_CI);
        rspCodes.put("108", Iso8583JSON.RspCode._02_REFER_TO_CI_SPECIAL);
        rspCodes.put("109", Iso8583JSON.RspCode._03_INVALID_MERCHANT);
        rspCodes.put("200", Iso8583JSON.RspCode._04_PICK_UP);
        rspCodes.put("100", Iso8583JSON.RspCode._05_DO_NOT_HONOUR);
        rspCodes.put("913", Iso8583JSON.RspCode._06_ERROR);
        rspCodes.put("207", Iso8583JSON.RspCode._07_PICK_UP_SPECIAL);
        rspCodes.put("001", Iso8583JSON.RspCode._08_HONOUR_WITH_ID);
        rspCodes.put("923", Iso8583JSON.RspCode._09_REQUEST_IN_PROGRESS);
        rspCodes.put("002", Iso8583JSON.RspCode._10_APPROVED_PARTIAL);
        rspCodes.put("003", "11");
        rspCodes.put("902", "12");
        rspCodes.put("110", "13");
        rspCodes.put("111", "14");
        rspCodes.put("908", "15");
        rspCodes.put("004", "16");
        rspCodes.put("903", "19");
        rspCodes.put("921", "21");
        rspCodes.put("909", "22");
        rspCodes.put("113", "23");
        rspCodes.put("301", "24");
        rspCodes.put("302", "25");
        rspCodes.put("308", "26");
        rspCodes.put("304", "27");
        rspCodes.put("305", "28");
        rspCodes.put("306", "29");
        rspCodes.put("904", Iso8583JSON.RspCode._30_FORMAT_ERROR);
        rspCodes.put("905", Iso8583JSON.RspCode._31_BANK_NOT_SUPPORTED);
        rspCodes.put("201", Iso8583JSON.RspCode._33_EXPIRED_CARD_PICK_UP);
        rspCodes.put("202", Iso8583JSON.RspCode._34_SUSPECTED_FRAUD_PICK_UP);
        rspCodes.put("203", Iso8583JSON.RspCode._35_CONTACT_ACQ_PICK_UP);
        rspCodes.put("204", Iso8583JSON.RspCode._36_RESTRICTED_CARD_PICK_UP);
        rspCodes.put("205", Iso8583JSON.RspCode._37_CALL_ACQ_SECURITY_PICK_UP);
        rspCodes.put("206", Iso8583JSON.RspCode._38_PIN_TRIES_EXCEEDED_PICK_UP);
        rspCodes.put("115", "40");
        rspCodes.put("208", "41");
        rspCodes.put("114", "42");
        rspCodes.put("209", "43");
        rspCodes.put("116", "51");
        rspCodes.put("101", "54");
        rspCodes.put("117", "55");
        rspCodes.put("118", "56");
        rspCodes.put("119", "57");
        rspCodes.put("120", "58");
        rspCodes.put("102", "59");
        rspCodes.put("103", "60");
        rspCodes.put("121", "61");
        rspCodes.put("104", "62");
        rspCodes.put("122", "63");
        rspCodes.put("110", "64");
        rspCodes.put("123", "65");
        rspCodes.put("105", "66");
        rspCodes.put("200", Iso8583JSON.RspCode._67_HARD_CAPTURE);
        rspCodes.put("911", "68");
        rspCodes.put("106", "75");
        rspCodes.put("103", "77");
        rspCodes.put("103", "78");
        rspCodes.put("906", "90");
        rspCodes.put("907", Iso8583JSON.RspCode._91_ISSUER_OR_SWITCH_INOPERATIVE);
        rspCodes.put("908", Iso8583JSON.RspCode._92_ROUTING_ERROR);
        rspCodes.put("124", Iso8583JSON.RspCode._93_VIOLATION_OF_LAW);
        rspCodes.put("913", Iso8583JSON.RspCode._94_DUPLICATE_TRANSMISSION);
        rspCodes.put("915", Iso8583JSON.RspCode._95_RECONCILE_ERROR);
        rspCodes.put("909", Iso8583JSON.RspCode._96_SYSTEM_MALFUNCTION);
        rspCodes.put("121", Iso8583JSON.RspCode._98_EXCEEDS_CASHLIMIT);
    }
}
