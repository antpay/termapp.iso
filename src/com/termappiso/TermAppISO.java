package com.termappiso;

import ap.sdk.comms.XHttpServer;
import ap.sdk.processor.BaseModule;
import ap.sdk.util.MQueue;
import ap.sdk.util.ProcessStatusUpdate;
import com.termappiso.message.ProcessMsgFromTM;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 *
 * @author abhishek.m
 */
public class TermAppISO extends BaseModule
{

    int listenPort;
    int backlog;
    String context;
    String processName;
    String ipaddress;
    long idletimeout;
    ExecutorService pool;

    /**
     *
     * @param moduleName
     */
    public TermAppISO(String moduleName)
    {
        super(new MQueue());

        Config.initializeConfigurations(moduleName);

        pool = Executors.newCachedThreadPool();
    }

    /**
     *
     */
    public void startprocess()
    {
        Connection conn;
        PreparedStatement stmt;
        ResultSet rs;

        // Loading of Issuer Node Data
        String selectSQL = "select listenip, listenport, "
                + "contextpath, backlog, pname, idletimeout from onl_process where pname = ?";
        try
        {
            conn = Config.dbm.getConnection();
            stmt = conn.prepareStatement(selectSQL);
            stmt.setString(1, Config._MODULE_NAME);
            rs = stmt.executeQuery();

            while (rs.next())
            {
                this.ipaddress = rs.getString("listenip");
                this.listenPort = rs.getInt("listenport");
                this.context = rs.getString("contextpath");
                this.backlog = rs.getInt("backlog");
                this.processName = rs.getString("pname");
                this.idletimeout = (long) rs.getInt("idletimeout");
            }

            selectSQL = "select ip, port, type, usessl, keystorepath "
                    + " from onl_ip_conf where proc_name=?";

            stmt = conn.prepareStatement(selectSQL);
            stmt.setString(1, Config._MODULE_NAME);
            rs = stmt.executeQuery();

            while (rs.next())
            {
                Config.remoteIP = rs.getString("ip");
                Config.remotePort = rs.getInt("port");
            }

            rs.close();
            stmt.close();
            conn.close();
        } catch (SQLException ex)
        {
            Config.logger.fatal(ex);
            System.exit(1);
        }

        try
        {
            Config.tmServer = new XHttpServer("TMClient", ipaddress, listenPort, getQueue(), backlog, idletimeout);
            Config.tmServer.startprocess();
        } catch (IOException ex)
        {
            Config.logger.fatal(ex);
            System.exit(1);
        }

        pool.execute(new ProcessStatusUpdate(Config._MODULE_NAME, Config.dbm));
        
        Config.logger.info("Started");

        start();
    }

    /**
     * 
     */
    public void shutdown()
    {
        pool.shutdownNow();

        Config.logger.info("Shutdown module ... " + Config._MODULE_NAME);
    }

    /**
     *
     * @param event
     */
    @Override
    public void processEvent(Object event)
    {
        if (event instanceof XHttpServer.DataEvent)
        {
            pool.execute(new ProcessMsgFromTM(event));
        }
    }

    /**
     * 
     * @param args 
     */
    public static void main(String[] args)
    {
        TermAppISO ti = new TermAppISO(args[0]);
        ti.startprocess();
    }
}
